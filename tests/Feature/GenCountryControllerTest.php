<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\GenCountry;


class GenCountryControllerTest extends TestCase
{


    /**
     * Inicializadores
     * El setUp se ejecuta en cada metodo de un test
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        factory(GenCountry::class)->create([
            'country' => 'El Salvador'
        ]);
        factory(GenCountry::class)->create([
            'country' => 'Guatemala'
        ]);


    }
   
    /*
     * Test 
     */
    public function testIndex()
    {
        //acierta si estan los dos registros iniciales y el estatus del response
        $response = $this->get('api/country')->assertStatus(200)->assertJsonCount(2);
    }

    public function testStore()
    {
        $this->json('POST', 'api/country', ['country' => 'Costa Rica'])->assertStatus(200);
        $this->assertDatabaseHas('genCountry', ['country' => 'Costa Rica']);
    }


    public function testShow()
    {
        $response = $this->get('api/country/2')->assertStatus(200)->assertJson([
            'country' => 'Guatemala'
        ]);

    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/country/1', ['country' => 'Honduras'])->assertStatus(200);
        $this->assertDatabaseHas('genCountry', ['country' => 'Honduras']);
    }

    public function testDestroy()
    {
        $this->delete('api/country/1')->assertStatus(200);
        $this->assertDatabaseMissing('genCountry', ['country' => 'El Salvador']);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM genCountry;');
        DB::statement('ALTER TABLE genCountry auto_increment=1;');
    }



}
