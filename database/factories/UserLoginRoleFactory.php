<?php

use Faker\Generator as Faker;

$factory->define(App\UserLoginRole::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->name,
        'active' => 1,
        'updatedById' => $faker->randomDigit,
        'createdById' => $faker->randomDigit
    ];
});
