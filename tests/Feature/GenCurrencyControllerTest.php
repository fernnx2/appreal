<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\GenCurrency;

class GenCurrencyControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        \factory(GenCurrency::class)->create([
            'code' => 'US',
            'name' => 'dollar',
            'symbol' => '$',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ]);
        \factory(GenCurrency::class)->create([
            'code' => 'EUR',
            'name' => 'Euro',
            'symbol' => 'e',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ]);
    }

    public function testIndex()
    {
        $this->get('api/currency')->assertStatus(200);
    }

    public function testStore()
    {
        $this->json('POST', 'api/currency', [
            'code' => 'AM',
            'name' => 'colon',
            'symbol' => 'c',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ])->assertStatus(200);
        $this->assertDatabaseHas('genCurrency', [
            'code' => 'AM',
            'name' => 'colon',
            'symbol' => 'c',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ]);
    }

    public function testShow()
    {
        $response = $this->get('api/currency/1')->assertStatus(200);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/currency/1', [
            'code' => 'en_US',
            'name' => 'dollar',
            'symbol' => '$',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ])->assertStatus(200);
        $this->assertDatabaseHas('genCurrency', [
            'code' => 'en_US',
            'name' => 'dollar',
            'symbol' => '$',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ]);
    }

    public function testDestroy()
    {
        $this->delete('api/currency/2')->assertStatus(200);
        $this->assertDatabaseMissing('genCurrency', [
            'code' => 'EUR',
            'name' => 'Euro',
            'symbol' => 'e',
            'currencyRounding' => 1,
            'decimalSeparator' => ',',
            'decimalThousand' => ',',
            'groupingSeparator' => ','
        ]);

    }

    public function tearDown()
    {
        DB::statement('DELETE FROM genCurrency;');
        DB::statement('ALTER TABLE genCurrency auto_increment=1;');
    }

}
