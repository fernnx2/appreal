<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoginRole extends Model
{
    protected $table = 'userLoginRole';
    protected $primaryKey = 'userLoginRoleId';
    public $timestamps = false;
}
