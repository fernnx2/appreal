<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisOfficeType extends Model
{
    protected $table = 'lisOfficeType';
    protected $primaryKey = 'lisOfficeTypeId';
    public $timestamps = false;
}
