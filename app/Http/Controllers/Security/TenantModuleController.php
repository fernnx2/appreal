<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;

use App\TenantModule;

class TenantModuleController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tenantModule = TenantModule::all();
            return response()->json($tenantModule, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantModule = new TenantModule();
            $tenantModule->name = $data['name'];
            $tenantModule->active = $data['active'];
            $tenantModule->updatedById = $data['updatedById'];
            $tenantModule->createdById = $data['createdById'];
            $tenantModule->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tenantModule = TenantModule::findOrFail($id);
            return response()->json($tenantModule, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenantModule = TenantModule::findOrFail($id);
        $validator = \Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['updated' => false, 'errors' => $validator->errors()->all()], 401);
        }
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantModule->name = $data['name'];
            $tenantModule->active = $data['active'];
            $tenantModule->updatedById = $data['updatedById'];
            $tenantModule->createdById = $data['createdById'];
            $tenantModule->modifyDateTime = now();
            $tenantModule->save();
            return response()->json('update', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tenantModule = TenantModule::findOrFail($id);
            $tenantModule->delete();
            return response()->json('delete', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
