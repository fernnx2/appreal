# Proyecto para Real App

# Inicio

Luego de clonar el repositorio, hacer un nuevo archivo llamado .env 


Copiar el ejemplo siguiente al archivo y configurarlo segun se convenga


#### Consideracion adicional -No cambiar el APP_KEY
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:IL7o9sKfDQ/xMCoUpXbbE5SkRQeH5p9GUyJmgf4fqRY=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=realapp
DB_USERNAME=user
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.sendgrid.net
MAIL_PORT=587
MAIL_USERNAME=apikey
MAIL_PASSWORD=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
MAIL_ENCRYPTION=tls

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRE

```

# Instalación

    ```
    composer install
    ```
## Levantar Server Local    
    ```
    php artisan serve
    ```

## Ruteo de la Api 

    ```
    php artisan route:list
    ```

# Configuracion del Email
Básicamente el envio del email ya estara configurado al declarar las
variables de  entorno en el archivo `.env` descrito al principio del este Readme.md

Sin Embargo si se require cambiar de proveedor se podran cambiar estos valores de este archivo
reemplazandolos por los nuevos valores.

Ademas de esto hay unas configuraciones con respecto al email enviado las cuales son:
    
    1. El Emisor y sus datos
    2. La vista del Cuerpo del Mensaje

Para cambiar de emisor y sus datos ir al archivo ubicado en /app/mail/UserRequestDemoSendEmail.php
Este archivo contiene un `contructor` 

que se encarga de obtener los datos del usuario, y un metodo
llamado `build` , en este metodo estan las configuraciones para los datos del emisor.

Son tres variables llamadas:
```
$address = 'devfernando95@gmail.com';
$subject = 'Mensaje de Confirmacion!';
$name = 'RealDeal';
```

Una vez aca, se pueden poner los valores deseados como `hardCode` o se puede también declarar estos 
valores como `variables` y pasarlas por el `contructor` si se deseara tener muchos Emisores.

De ser haci debera ir al archivo llamado `SendEmailController.php` ubicado en `App/Http/` en el 
metodo `show()` y ahi en la siguiente linea 

```
Mail::to($emailAddress)->send(new UserRequestDemoSendEmail($user));
```

Agregarle los parametros al objeto UserRequestDemoSendEmail($user);


# Testing
Para testear la aplicacion se creo un ambiente aislado al de producción,

por ende se debera crear la base de datos correspondiente para esto

llamada `realapptest`

luego de esto se podran ejecutar todos los test con el comando

    ```
    vendor/bin/phpunit
    ```

 Puede tambien crear un comando alias para su facil utilización, este comando estara

funcionando durante la session en la cli (o hasta que cli sea cerrada).

Comando para crear un alias 

    ```
    alias runtest='vendor/bin/phpunit'
    ```

Luego de Esto puede Correr el comando:

    ```
    runtest
    ```

Si se desea ejecutar una prueba en concreto puede ejecutar

    ```
    runtest --filter [nombreMetodo] [RutaDelArchivo]
    ```
Ejemplo

    ```
    runtest --filter testIndex ./test/Feature/EmployeeTypeControllerTest
    ```

### Consideraciones de Testeo
 Asegurarse que la bd esta corriendo y que hayan sido creadas sus tablas

 con los scripts proporcionados en el repositorio `https://gitlab.com/fernnx2/bdrealapp`

 llamados `bd.sql` y `constraint.sql` .

 Se considero un ambiente de pruebas real con transacciones para una mejor calidad.
 


# Lenguage de Programacion y Entorno de Trabajo
 
 * Aplicación programada en PHP7 
 * Framework Laravel 5.5

 
# Dependencias 

1. Tener Instalado de Manera `Global` en su sistema anfitrión, o en un server `Local` o 
   `Remmoto` `php7` como mínimo.
2. Tener Instalado en gestor de Dependencias `Composer`  

# Arquitectura de Proyecto
  1. Segun la documentación oficial de Laravel los modelos `Entidades o Pojos`, deberan
  estar en la raiz de la carpeta `App`.

  Ahí estarán todos los modelos de la aplicación.

  2. Los `Controladores` para cada modelo, estarán definidos segun su `modulo` de la base
  de datos.

  Y estos estarán en `App/Http/Controllers` , seguido de el nombre del modulo correspondiente.

  3. El ruteo se ubica el la carpeta `routes` especificamente en el archivo `api.php`.

# Exclusiones de Arquitectura

  Como se trabajara de manera independiente entre el `Front-End` y el `Back-End`

  No se resolveran vistas en este proyecto destinado al `Back-End` ya que solo se expondra la Api RestFull.

# Comentario
   En cuanto se valla avanzando en el proyecto, se irán dando mas especificaciones
   segun convenga.
  

## License Laravel

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
