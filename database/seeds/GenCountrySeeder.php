<?php

use Illuminate\Database\Seeder;

class GenCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genCountry')->insert([
            'country' => 'Guatemala'
        ]);
        DB::table('genCountry')->insert([
            'country' => 'Costa Rica'
        ]);
        DB::table('genCountry')->insert([
            'country' => 'Honduras'
        ]);
        DB::table('genCountry')->insert([
            'country' => 'Panama'
        ]);
    }
}
