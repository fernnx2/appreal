<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfigNameSubGroup;
use App\Http\Util\ValidatorController;

class GenConfigNameSubGroupController extends Controller
{
    private $rules = [
        'active' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $configNameSubGroup = GenConfigNameSubGroup::all();
            return response()->json($configNameSubGroup, 200);
        } catch (\PDOException $th) {
            return response()->json(["findAll" => "Fail"], 400);
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configNameSubGroup = new GenConfigNameSubGroup();
            $configNameSubGroup->genConfigNameGroupId = $data['genConfigNameGroupId'];
            $configNameSubGroup->name = $data['name'];
            $configNameSubGroup->active = $data['active'];
            $configNameSubGroup->updatedById = $data['updatedById'];
            $configNameSubGroup->createdById = $data['createdById'];
            $configNameSubGroup->isVisible = $data['isVisible'];
            $configNameSubGroup->save();
            $genConfigNameSubGroupId = $configNameSubGroup->genConfigNameSubGroupId;
            return response()->json(["genConfigNameSubGroupId", $genConfigNameSubGroupId], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $configNameSubGroup = GenConfigNameSubGroup::findOrFail($id);
            return response()->json($configNameSubGroup, 200);
        } catch (\PDOException $th) {
            return response()->json(["find" => "Fail"], 400);
        }
    }



    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $configNameSubGroup = GenConfigNameSubGroup::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configNameSubGroup->genConfigNameGroupId = $data['genConfigNameGroupId'];
            $configNameSubGroup->name = $data['name'];
            $configNameSubGroup->active = $data['active'];
            $configNameSubGroup->updatedById = $data['updatedById'];
            $configNameSubGroup->createdById = $data['createdById'];
            $configNameSubGroup->modifyDateTime = now();
            $configNameSubGroup->isVisible = $data['isVisible'];
            $configNameSubGroup->save();
            return response()->json(["updated", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["updated" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $configNameSubGroup = GenConfigNameSubGroup::findOrFail($id);
            $configNameSubGroup->delete();
            return response()->json(["delete" => "Exito"], 200);
        } catch (\PDOException $th) {
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
