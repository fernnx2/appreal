<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenTimezone;
use App\Http\Util\ValidatorController;

class GenTimezoneController extends Controller
{
    private $rules = [
        'timeZone' => 'required',
        'createdById' => 'required',
        'genLanguageId' => 'required',
        'timezoneKey' => 'required',
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genTimezone = GenTimezone::all();
            return response()->json($genTimezone, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genTimezone = new GenTimezone();
            $genTimezone->timeZone = $data['timeZone'];
            $genTimezone->createdById = $data['createdById'];
            $genTimezone->genLanguageId = $data['genLanguageId'];
            $genTimezone->timezoneKey = $data['timezoneKey'];
            $genTimezone->save();
            return response()->json(["create", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genTimezone = GenTimezone::findOrFail($id);
        try {
            return response()->json($genTimezone, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Fail"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $genTimezone = GenTimezone::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genTimezone->timeZone = $data['timeZone'];
            $genTimezone->createdById = $data['createdById'];
            $genTimezone->genLanguageId = $data['genLanguageId'];
            $genTimezone->timezoneKey = $data['timezoneKey'];
            $genTimezone->modifyDateTime = now();
            $genTimezone->save();
            return response()->json(["update", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genTimezone = GenTimezone::findOrFail($id);
        try {
            $genTimezone->delete();
            return response()->json(["delete", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
