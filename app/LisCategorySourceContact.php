<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisCategorySourceContact extends Model
{
    protected $table = 'lisCategorySourceContact';
    protected $primaryKey = 'lisCategorySourceContactId';
    public $timestamps = false;
}
