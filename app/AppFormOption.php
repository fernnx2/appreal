<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppFormOption extends Model
{
    protected $table = 'appFormOption';
    protected $primaryKey = 'appFormOptionId';
    public $timestamps = false;
}
