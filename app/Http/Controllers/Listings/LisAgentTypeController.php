<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisAgentType;
use App\Http\Util\ValidatorController;

class LisAgentTypeController extends Controller
{
    private $rules = [
        'agentType' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $agentType = LisAgentType::all();
            return response()->json($agentType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'fail'], 400);
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agentType = new LisAgentType();
            $agentType->agentType = $data['agentType'];
            $agentType->description = $data['description'];
            $agentType->updatedById = $data['updatedById'];
            $agentType->createdById = $data['createdById'];
            $agentType->save();
            return response()->json($agentType, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $agentType = LisAgentType::findOrFail($id);
            return response()->json($agentType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agentType = LisAgentType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agentType->agentType = $data['agentType'];
            $agentType->description = $data['description'];
            $agentType->updatedById = $data['updatedById'];
            $agentType->createdById = $data['createdById'];
            $agentType->save();
            return response()->json($agentType, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $agentType = LisAgentType::findOrFail($id);
            $agentType->delete();
            return response()->json($agentType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'fail'], 400);
        }
    }
}
