<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserLogin;
use App\Http\Util\ValidatorController;

class UserLoginController extends Controller
{
    private $rules = [
        'userLoginTypeId' => 'required',
        'email' => 'required',
        'password' => 'required',
        'password2' => 'required',
        'confirmKey' => 'required',
        'isLocked' => 'required',
        'active' => 'required',
        'tenantId' => 'required',
        'createdById' => 'required',
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $userlogin = UserLogin::all();
            return response()->json($userlogin, 200);
        } catch (\PDOException $th) {
            return response()->json(['finAll' => 'Error!'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $userlogin = new UserLogin();
            $userlogin->userLoginTypeId = $data['userLoginTypeId'];
            $userlogin->contactId = $data['contactId'];
            $userlogin->userName = $data['userName'];
            $userlogin->email = $data['email'];
            $userlogin->password = $data['password'];
            $userlogin->password2 = $data['password2'];
            $userlogin->iterations = $data['iterations'];
            $userlogin->confirmKey = $data['confirmKey'];
            $userlogin->secretQuestion = $data['secretQuestion'];
            $userlogin->secretAnswer = $data['secretAnswer'];
            $userlogin->offset = $data['offset'];
            $userlogin->lastLoginDateTime = $data['lastLoginDateTime'];
            $userlogin->lastPasswordReset = $data['lastPasswordReset'];
            $userlogin->isLocked = $data['isLocked'];
            $userlogin->active = $data['active'];
            $userlogin->tenantId = $data['tenantId'];
            $userlogin->updatedById = $data['updatedById'];
            $userlogin->createdById = $data['createdById'];
            $userlogin->createdPortalBillingLink = $data['createdPortalBillingLink'];
            $userlogin->portalBillingLink = $data['portalBillingLink'];
            $userlogin->external_userId = $data['external_userId'];
            $userlogin->tourIsEnded = $data['tourIsEnded'];
            $userlogin->isGuest = $data['isGuest'];
            $userlogin->save();
            return response()->json(['create' => 'Exito!'], 200);
        } catch (\PDOException $th) {
            return response()->json($th, 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userlogin = UserLogin::where('userName', $id)->firstOrFail();
            return response()->json($userlogin, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'Error!'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userlogin = UserLogin::findOrFail($id);
        $validator = \Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['created' => false, 'errors' => $validator->errors()->all()], 401);
        }
        try {
            $data = \json_decode($request->getContent(), true);
            $userlogin->userLoginTypeId = $data['userLoginTypeId'];
            $userlogin->contactId = $data['contactId'];
            $userlogin->userName = $data['userName'];
            $userlogin->email = $data['email'];
            $userlogin->password = $data['password'];
            $userlogin->password2 = $data['password2'];
            $userlogin->iterations = $data['iterations'];
            $userlogin->confirmKey = $data['confirmKey'];
            $userlogin->secretQuestion = $data['secretQuestion'];
            $userlogin->secretAnswer = $data['secretAnswer'];
            $userlogin->offset = $data['offset'];
            $userlogin->lastLoginDateTime = $data['lastLoginDateTime'];
            $userlogin->lastPasswordReset = $data['lastPasswordReset'];
            $userlogin->isLocked = $data['isLocked'];
            $userlogin->active = $data['active'];
            $userlogin->tenantId = $data['tenantId'];
            $userlogin->updatedById = $data['updatedById'];
            $userlogin->createdById = $data['createdById'];
            $userlogin->createdPortalBillingLink = $data['createdPortalBillingLink'];
            $userlogin->portalBillingLink = $data['portalBillingLink'];
            $userlogin->external_userId = $data['external_userId'];
            $userlogin->tourIsEnded = $data['tourIsEnded'];
            $userlogin->isGuest = $data['isGuest'];
            $userlogin->save();
            return response()->json(['create' => 'Exito!'], 200);
        } catch (\PDOException $th) {
            return response()->json($th, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $userlogin = UserLogin::findOrFail($id);
            $userlogin->delete();
            return response()->json(['delete' => 'Exito!'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Error!'], 400);
        }
    }
}
