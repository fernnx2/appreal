<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisRoms;
use App\Http\Util\ValidatorController;

class LisRomsController extends Controller
{
    private $rules = [
        'name' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roms = LisRoms::all();
            return response()->json($roms, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $rom = new LisRoms();
            $rom->name = $data['name'];
            $rom->description = $data['description'];
            $rom->updatedById = $data['updatedById'];
            $rom->createdById = $data['createdById'];
            $rom->save();
            return response()->json($rom, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $rom = LisRoms::findOrFail($id);
            return response()->json($rom, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rom = LisRoms::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $rom->name = $data['name'];
            $rom->description = $data['description'];
            $rom->updatedById = $data['updatedById'];
            $rom->createdById = $data['createdById'];
            $rom->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $rom = LisRoms::findOrFail($id);
            $rom->delete();
            return response()->json(['Delete' => 'Fail'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }
}
