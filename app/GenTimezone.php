<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenTimezone extends Model
{
    protected $table = 'genTimezone';
    protected $primaryKey = 'genTimezoneId';
    public $timestamps = false;
}
