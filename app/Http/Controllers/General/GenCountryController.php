<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenCountry;
use App\Http\Util\ValidatorController;

class GenCountryController extends Controller
{

    private $rules = [
        'country' => 'required'
    ];

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genCountry = GenCountry::all();
            return response()->json($genCountry, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genCountry = new GenCountry();
            $genCountry->country = $data['country'];
            $genCountry->save();
            return response()->json(["created", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => 'Fail'], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genCountry = GenCountry::findOrFail($id);
        try {
            return response()->json($genCountry, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findById" => "Fail"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $genCountry = GenCountry::find($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genCountry->country = $data['country'];
            $genCountry->modifyDateTime = now();
            $genCountry->save();
            return response()->json(["update", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genCountry = GenCountry::findOrFail($id);
        try {
            $genCountry->delete();
            return response()->json(["delete", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
