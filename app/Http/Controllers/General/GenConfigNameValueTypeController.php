<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfigNameValueType;
use App\Http\Util\ValidatorController;

class GenConfigNameValueTypeController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];


    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $configNameValueType = GenConfigNameValueType::all();
            return response()->json($configNameValueType, 200);
        } catch (\PDOException $th) {
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configNameValueType = new GenConfigNameValueType();
            $configNameValueType->name = $data['name'];
            $configNameValueType->active = $data['active'];
            $configNameValueType->updatedById = $data['updatedById'];
            $configNameValueType->createdById = $data['createdById'];
            $configNameValueType->save();
            $genConfigNameValueTypeId = $configNameValueType->genConfigNameValueTypeId;
            return response()->json(["genConfigNameValueTypeId", $genConfigNameValueTypeId], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $configNameValueType = GenConfigNameValueType::finOrFail($id);
            return response()->json($configNameValueType, 200);
        } catch (\PDOException $th) {
            return response()->json(["find" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $configNameValueType = GenConfigNameValueType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configNameValueType->name = $data['name'];
            $configNameValueType->active = $data['active'];
            $configNameValueType->updatedById = $data['updatedById'];
            $configNameValueType->createdById = $data['createdById'];
            $configNameValueType->modifyDateTime = now();
            $configNameValueType->save();
            return response()->json(["updated", "Exito"], 200);
        } catch (\PDOException $th) {
            return response()->json(["updated" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $configNameValueType = GenConfigNameValueType::finOrFail($id);
            $configNameValueType->delete();
            return response()->json(["delete" => "Exito"], 200);
        } catch (\PDOException $th) {
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
