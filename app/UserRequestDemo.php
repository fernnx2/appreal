<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestDemo extends Model
{
    protected $table = 'userRequestDemo';
    protected $primaryKey = 'userRequestDemoId';
    public $timestamps = false;

}
