<?php

use Faker\Generator as Faker;
use App\Agents;

$factory->define(Agents::class, function (Faker $faker) {
    return [
        'firstName' => $faker->name,
        'lastName' => $faker->name,
        'mlsid' => $faker->randomDigit,
        'miscid' => $faker->randomDigit,
        'fax' => $faker->name,
        'email' => $faker->email,
        'text' => $faker->text,
        'webpage' => $faker->name,
        'active' => 1,
        'address' => $faker->name,
        'notes' => $faker->realText($maxNbChars = 100, $indexSize = 2)

    ];
});
