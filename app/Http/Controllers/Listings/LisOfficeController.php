<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\LisOffice;

class LisOfficeController extends Controller
{
    private $rules = [
        'officeTypeId' => 'required',
        'tenantId' => 'required',
        'stateId' => 'required',
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'address' => 'required',
        'city' => ' required',
        'zipcode' => 'required',
        'country' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $offices = LisOffice::all();
            return response()->json($offices, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $office = new LisOffice();
            $office->officeTypeId = $data['officeTypeId'];
            $office->stateId = $data['stateId'];
            $office->tenantId = $data['tenantId'];
            $office->name = $data['name'];
            $office->phone = $data['phone'];
            $office->fax = $data['fax'];
            $office->email = $data['email'];
            $office->webpage = $data['webpage'];
            $office->address = $data['address'];
            $office->city = $data['city'];
            $office->zipcode = $data['zipcode'];
            $office->country = $data['country'];
            $office->notes = $data['notes'];
            $office->updatedById = $data['updatedById'];
            $office->createdById = $data['createdById'];
            $office->save();
            return response()->json($office, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $offices = LisOffice::findOrFail($id);
            return response()->json($offices, 200);
        } catch (\PDOException $th) {
            return response()->json(['Find' => 'Fail'], 400);
        }
    }

    public function findByTenant($id)
    {
        try {
            $offices = LisOffice::where('tenantId', $id)->get();
            return response()->json($offices, 200);
        } catch (\PDOException $th) {
            return response()->json(['Find' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $office = LisOffice::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $office->officeTypeId = $data['officeTypeId'];
            $office->stateId = $data['stateId'];
            $office->tenantId = $data['tenantId'];
            $office->name = $data['name'];
            $office->phone = $data['phone'];
            $office->fax = $data['fax'];
            $office->email = $data['email'];
            $office->webpage = $data['webpage'];
            $office->address = $data['address'];
            $office->city = $data['city'];
            $office->zipcode = $data['zipcode'];
            $office->country = $data['country'];
            $office->notes = $data['notes'];
            $office->updatedById = $data['updatedById'];
            $office->createdById = $data['createdById'];
            $office->modifyDateTime = now();
            $office->save();
            return response()->json($office, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $offices = LisOffice::findOrFail($id);
            $offices->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
