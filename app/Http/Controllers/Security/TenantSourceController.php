<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TenantSource;
use App\Http\Util\ValidatorController;

class TenantSourceController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tenantSource = TenantSource::all();
            return response()->json($tenantSource, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantSource = new TenantSource();
            $tenantSource->name = $data['name'];
            $tenantSource->active = $data['active'];
            $tenantSource->updatedById = $data['updatedById'];
            $tenantSource->createdById = $data['createdById'];
            $tenantSource->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tenantSource = TenantSource::findOrFail($id);
            return response()->json($tenantSource, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenantSource = TenantSource::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantSource->name = $data['name'];
            $tenantSource->active = $data['active'];
            $tenantSource->updatedById = $data['updatedById'];
            $tenantSource->createdById = $data['createdById'];
            $tenantSource->modifyDateTime = now();
            $tenantSource->save();
            return response()->json('update', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tenantSource = TenantSource::findOrFail($id);
            $tenantSource->delete();
            return response()->json('delete', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
