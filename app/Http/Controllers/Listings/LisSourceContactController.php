<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisSourceContact;
use App\Http\Util\ValidatorController;

class LisSourceContactController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'source' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $source = LisSourceContact::all();
            return response()->json($source, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $source = new LisSourceContact();
            $source->source = $data['source'];
            $source->tenantId = $data['tenantId'];
            $source->description = $data['description'];
            $source->updatedById = $data['updatedById'];
            $source->createdById = $data['createdById'];
            $source->save();
            return response()->json($source, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $source = LisSourceContact::findOrFail($id);
            return response()->json($source, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $source = LisSourceContact::where('tenantId', $id)->get();
            return response()->json($source, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $source = LisSourceContact::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $source->source = $data['source'];
            $source->tenantId = $data['tenantId'];
            $source->description = $data['description'];
            $source->updatedById = $data['updatedById'];
            $source->createdById = $data['createdById'];
            $source->modifyDateTime = now();
            $source->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $source = LisSourceContact::findOrFail($id);
            $source->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
