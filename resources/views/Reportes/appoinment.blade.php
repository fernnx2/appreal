@extends('layout.layout')

@section('content')
<h1>Appoinment</h1>
<ul>
    <li>{{$appoinment[0]->date}}</li>
    <li>{{$appoinment[0]->scheduledBy}}</li>
    <li>{{$appoinment[0]->scheduledOn}}</li>
    <li>{{$appoinment[0]->duration}}</li>
    <li>{{$appoinment[0]->notes}}</li>
    <li>{{$appoinment[0]->type}}</li>
    <li>{{$appoinment[0]->status}}</li>
    <li>{{$agent[0]->firstName}}</li>
    <li>{{$agent[0]->lastName}}</li>
    <li>{{$call[0]->reasonCall}}</li>
    <li>{{$call[0]->firstName}}</li>
    <li>{{$agent[0]->lastName}}</li>
    <li>{{$agent[0]->email}}</li>
</ul>
@endsection