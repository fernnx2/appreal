<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AppForm;
use App\Http\Util\ValidatorController;

class AppFormController extends Controller
{
    private $rules = [
        'correlative' => 'required',
        'forms' => 'required',
        'formName' => 'required',
        'isConfigValue' => 'required',
        'isManyForms' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $app = AppForm::all();
            return response()->json($app, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $app = new AppForm();
            $app->tenantModuleId = $data['tenantModuleId'];
            $app->correlative = $data['correlative'];
            $app->forms = $data['forms'];
            $app->formName = $data['formName'];
            $app->isConfigValue = $data['isConfigValue'];
            $app->isManyForms = $data['isManyForms'];
            $app->storedProcedureName = $data['storedProcedureName'];
            $app->tableName = $data['tableName'];
            $app->image = $data['image'];
            $app->imageURL = $data['imageURL'];
            $app->urlForm = $data['urlForm'];
            $app->sortId = $data['sortId'];
            $app->active = $data['active'];
            $app->updatedById = $data['updatedById'];
            $app->createdById = $data['createdById'];
            $app->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $app = AppForm::findOrFail($id);
            return response()->json($app, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $app = AppForm::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $app->tenantModuleId = $data['tenantModuleId'];
            $app->correlative = $data['correlative'];
            $app->forms = $data['forms'];
            $app->formName = $data['formName'];
            $app->isConfigValue = $data['isConfigValue'];
            $app->isManyForms = $data['isManyForms'];
            $app->storedProcedureName = $data['storedProcedureName'];
            $app->tableName = $data['tableName'];
            $app->image = $data['image'];
            $app->imageURL = $data['imageURL'];
            $app->urlForm = $data['urlForm'];
            $app->sortId = $data['sortId'];
            $app->active = $data['active'];
            $app->updatedById = $data['updatedById'];
            $app->createdById = $data['createdById'];
            $app->modifyDateTime = now();
            $app->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $app = AppForm::findOrFail($id);
            $app->delete();
            return response()->json(["delete" => "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
