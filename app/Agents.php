<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agents extends Model
{
    protected $table = 'agents';
    protected $primaryKey = 'agentsId';
    public $timestamps = false;
}
