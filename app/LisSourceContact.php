<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisSourceContact extends Model
{
    protected $table = 'lisSourceContact';
    protected $primaryKey = 'lisSourceContactId';
    public $timestamps = false;
}
