<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LisAgent;

class LisAgentType extends Model
{
    protected $table = 'lisAgentType';
    protected $primaryKey = 'lisAgentTypeId';
    public $timestamps = false;


}
