<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisAgenthasProperty extends Model
{
    protected $table = 'lisAgenthasProperty';
    protected $primaryKey = 'lisAgenthasPropertyId';
    public $timestamps = false;
}
