<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfigName extends Model
{
    protected $table = 'genConfigName';
    protected $primaryKey = 'genConfigNameId';
    public $timestamps = false;
}
