<?php

use Illuminate\Database\Seeder;

class GenTimezoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genTimezone')->insert([
            'timeZone' => 'central',
            'updatedById' => '1',
            'createdById' => '1',
            'insertDateTime' => now(),
            'timezoneKey' => '1'
        ]);
        DB::table('genTimezone')->insert([
            'timeZone' => 'paracentral',
            'createdById' => '1',
            'createdById' => '1',
            'insertDateTime' => now(),
            'timezoneKey' => '2'
        ]);
    }
}
