<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\ContactPhoneType;

class ContactPhoneTypeController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $contacPhoneType = ContactPhoneType::all();
            return response()->json($contacPhoneType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $contactPhoneType = new contactPhoneType();
            $contactPhoneType->name = $data['name'];
            $contactPhoneType->active = $data['active'];
            $contactPhoneType->updatedById = $data['updatedById'];
            $contactPhoneType->createdById = $data['createdById'];
            $contactPhoneType->save();
            return response()->json($contactPhoneType, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $contacPhoneType = ContactPhoneType::findOrFail($id);
            return response()->json($contacPhoneType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contactPhoneType = contactPhoneType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $contactPhoneType->name = $data['name'];
            $contactPhoneType->active = $data['active'];
            $contactPhoneType->updatedById = $data['updatedById'];
            $contactPhoneType->createdById = $data['createdById'];
            $contactPhoneType->save();
            return response()->json($contactPhoneType, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => $th], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $contacPhoneType = ContactPhoneType::findOrFail($id);
            $contacPhoneType->delete();
            return response()->json($contacPhoneType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }
}
