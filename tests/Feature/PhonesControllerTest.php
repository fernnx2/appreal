<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PhoneType;
use App\Phones;

class PhonesControllerTest extends TestCase
{
    public $phones = [
        ['number' => '(503) 77343577', 'phoneTypeId' => 1],
        ['number' => '(502) 12121212', 'phoneTypeId' => 2]
    ];
    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(PhoneType::class, 2)->create();
        factory(Phones::class)->create(['number' => '(503) 77343577', 'phoneTypeId' => 1]);
        factory(Phones::class)->create(['number' => '22224444', 'phoneTypeId' => 2]);
    }

    public function testStore()
    {
        $this->json('POST', 'api/phones', $this->phones)->assertStatus(200);

    }

    public function tearDown()
    {
        DB::statement('DELETE FROM phones;');
        DB::statement('ALTER TABLE phones auto_increment=1;');
    }
}
