<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisStatus extends Model
{
    protected $table = 'lisStatus';
    protected $primaryKey = 'lisStatusId';
    public $timestamps = false;
}
