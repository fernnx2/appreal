<?php

namespace App\Http\Controllers\UserMail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\Mail\UserFailEmailExist;
use App\UserRequestDemo;

class UserFailEmailExistController extends Controller
{
    public function show($id)
    {
        try {
            $user = UserRequestDemo::where('email', '=', $id)->firstOrFail();
            $emailAddress = $user['email'];
            Mail::to($emailAddress)->send(new UserFailEmailExist($user));
            return response()->json(['status send mail' => 'Email enviado Exitosamente'], 200);
        } catch (\PDOException $th) {
            return response()->json(['status send email' => 'Fail'], 400);
        }
    }


}
