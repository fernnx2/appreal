<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\TenantModule;

class TenantModuleControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public $tenantmodule = [
        'name' => 'test',
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1
    ];
    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(TenantModule::class)->create(['name' => 'module']);
        factory(TenantModule::class, 5)->create();
    }

    public function testIndex()
    {
        $this->get('api/tenantmodule')->assertStatus(200)->assertJsonCount(6);
    }

    public function testStore()
    {
        $this->json('POST', 'api/tenantmodule', $this->tenantmodule)->assertStatus(200);
        $this->assertDatabaseHas('tenantModule', $this->tenantmodule);
    }

    public function testShow()
    {
        $this->get('api/tenantmodule/1')->assertStatus(200)->assertJson(['name' => 'module']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/tenantmodule/1', $this->tenantmodule)->assertStatus(200);
        $this->assertDatabaseHas('tenantModule', $this->tenantmodule);
    }

    public function testDestroy()
    {
        $this->delete('api/tenantmodule/1')->assertStatus(200);
        $this->assertDatabaseMissing('tenantModule', ['name' => 'module']);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM tenantModule;');
        DB::statement('ALTER TABLE tenantModule auto_increment=1;');
    }
}
