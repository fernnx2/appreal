<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\TenantStatus;

class TenantStatusControllerTest extends TestCase
{
    private $tenantStatus = [
        'name' => 'disable',
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1
    ];
    /**
     * 
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(TenantStatus::class)->create(['name' => 'activo']);
        factory(TenantStatus::class, 3)->create();
    }
    public function testIndex()
    {
        $this->get('api/tenantstatus')->assertStatus(200)->assertJsonCount(4);
    }
    public function testStore()
    {
        $this->json('POST', 'api/tenantstatus', $this->tenantStatus)->assertStatus(200);
        $this->assertDatabaseHas('tenantStatus', $this->tenantStatus);
    }

    public function testShow()
    {
        $this->get('api/tenantstatus/1')->assertStatus(200)->assertJson(['name' => 'activo']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/tenantstatus/1', $this->tenantStatus)->assertStatus(200);
        $this->assertDatabaseHas('tenantStatus', $this->tenantStatus);
    }

    public function testDestroy()
    {
        $this->delete('api/tenantstatus/1 ')->assertStatus(200);
        $this->assertDatabaseMissing('tenantStatus', ['name' => 'activo']);
    }



    public function tearDown()
    {
        DB::statement(' DELETE FROM tenantStatus;
        ');
        DB::statement(' ALTER TABLE tenantStatus auto_increment = 1;');
    }

}
