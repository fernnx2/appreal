<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfigNameGroup;
use App\Http\Util\ValidatorController;

class GenConfigNameGroupController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $configNameGroup = GenConfigNameGroup::all();
            return response()->json($configNameGroup, 200);
        } catch (\PDOException $th) {
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configNameGroup = new GenConfigNameGroup();
            $configNameGroup->name = $data['name'];
            $configNameGroup->active = $data['active'];
            $configNameGroup->updatedById = $data['updatedById'];
            $configNameGroup->createdById = $data['createdById'];
            $configNameGroup->isVisible = $data['isVisible'];
            $configNameGroup->save();
            $genConfigNameGroupId = $configNameGroup->genConfigNameGroupId;
            return response()->json(["genConfigNameGroupId", $genConfigNameGroupId], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $configNameGroup = GenConfigNameGroup::findOrFail($id);
            return response()->json($configNameGroup, 200);
        } catch (\PDOException $th) {
            return response()->json(["find" => "Fail"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $configNameGroup = GenConfigNameGroup::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configNameGroup->name = $data['name'];
            $configNameGroup->active = $data['active'];
            $configNameGroup->updatedById = $data['updatedById'];
            $configNameGroup->createdById = $data['createdById'];
            $configNameGroup->modifyDateTime = now();
            $configNameGroup->isVisible = $data['isVisible'];
            $configNameGroup->save();
            return response()->json(["created", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $configNameGroup = GenConfigNameGroup::findOrFail($id);
            $configNameGroup->delete();
            return response()->json(["delete" => "Exito"], 200);
        } catch (\PDOException $th) {
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
