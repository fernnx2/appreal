<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoginType extends Model
{
    protected $table = 'userLoginType';
    protected $primaryKey = 'userLoginTypeId';
    public $timestamps = false;
}
