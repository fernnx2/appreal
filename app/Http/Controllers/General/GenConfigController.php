<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfig;
use App\Http\Util\ValidatorController;

class GenConfigController extends Controller
{
    private $rules = [
        'genConfigNameId' => 'required',
        'parameter' => 'required',
        'active' => 'required',
        'tenantId' => 'required',
        'createdById' => 'required'

    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $config = GenConfig::all();
            return response()->json($config, 200);
        } catch (\PDOException $th) {
            return response()->json(["findAll" => "Error!"], 400);
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $config = new GenConfig();
            $config->genConfigNameId = $data['genConfigNameId'];
            $config->parameter = $data['parameter'];
            $config->active = $data['active'];
            $config->tenantId = $data['tenantId'];
            $config->updatedById = $data['updatedById'];
            $config->createdById = $data['createdById'];
            $config->save();
            $genConfigId = $config->genConfigId;
            return response()->json(["genConfigId", $genConfigId], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $config = GenConfig::findOrFail($id);
            return response()->json($config, 200);
        } catch (\PDOException $th) {
            return response()->json(["find" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $config = GenConfig::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $config->genConfigNameId = $data['genConfigNameId'];
            $config->parameter = $data['parameter'];
            $config->active = $data['active'];
            $config->tenantId = $data['tenantId'];
            $config->updatedById = $data['updatedById'];
            $config->createdById = $data['createdById'];
            $config->modifyDateTime = now();
            $config->save();
            return response()->json(["updated", 'Exito!'], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["updated" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $config = GenConfig::findOrFail($id);
            $config->delete();
            return response()->json(["delete" => "Exito!"], 200);
        } catch (\PDOException $th) {
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
