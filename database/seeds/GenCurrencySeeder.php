<?php

use Illuminate\Database\Seeder;

class GenCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genCurrency')->insert([
            'code' => '1',
            'name' => 'dollar',
            'symbol' => '$',
            'currencyRounding' => '1',
            'decimalSeparator' => '.',
            'decimalThousand' => '.',
            'groupingSeparator' => '.'
        ]);
        DB::table('genCurrency')->insert([
            'code' => '2',
            'name' => 'Quetzal',
            'symbol' => 'Q',
            'currencyRounding' => '1',
            'decimalSeparator' => '.',
            'decimalThousand' => '.',
            'groupingSeparator' => '.'
        ]);
    }
}
