<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenLanguage;
use App\Http\Util\ValidatorController;

class GenLanguageController extends Controller
{
    private $rules = [
        'code' => 'required',
        'name' => 'required',
        'active' => 'required',
        'culturename' => 'required',
        'cultureid' => 'required',
        'createdById' => 'required',

    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genLanguage = GenLanguage::all();
            return response()->json($genLanguage, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genLanguage = new GenLanguage();
            $genLanguage->code = $data['code'];
            $genLanguage->name = $data['name'];
            $genLanguage->active = $data['active'];
            $genLanguage->culturename = $data['culturename'];
            $genLanguage->cultureid = $data['cultureid'];
            $genLanguage->createdById = $data['createdById'];
            $genLanguage->updatedById = $data['updatedById'];
            $genLanguage->save();
            return response()->json(["create", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genLanguage = GenLanguage::findOrFail($id);
        try {
            return response()->json($genLanguage, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findById" => "Fail"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $genLanguage = GenLanguage::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genLanguage->code = $data['code'];
            $genLanguage->name = $data['name'];
            $genLanguage->active = $data['active'];
            $genLanguage->culturename = $data['culturename'];
            $genLanguage->cultureid = $data['cultureid'];
            $genLanguage->createdById = $data['createdById'];
            $genLanguage->updatedById = $data['updatedById'];
            $genLanguage->modifyDateTime = now();
            $genLanguage->save();
            return response()->json(["delete", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genLanguage = GenLanguage::findOrFail($id);
        try {
            $genLanguage->delete();
            return response()->json(["delete", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
