<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\TenantSubscriptionPlan;

class TenantSubscriptionPlanControllerTest extends TestCase
{
    private $tenansubscription = [
        'code' => '1234',
        'name' => 'pro',
        'active' => 1,
        'durationDays' => 30,
        'updatedById' => null,
        'createdById' => 1
    ];
    /**
     * 
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(TenantSubscriptionPlan::class)->create(['name' => 'premiun']);
        factory(TenantSubscriptionPlan::class)->create(['name' => 'Gratis']);
        factory(TenantSubscriptionPlan::class)->create(['name' => 'plus']);

    }

    public function testIndex()
    {
        $this->get('api/tenantsubscriptionplan')->assertStatus(200)->assertJsonCount(3);
    }

    public function testStore()
    {
        $this->json('POST', 'api/tenantsubscriptionplan', $this->tenansubscription)->assertStatus(200);
        $this->assertDatabaseHas('tenantSubscriptionPlan', $this->tenansubscription);
    }
    public function testShow()
    {
        $this->get('api/tenantsubscriptionplan/2')->assertStatus(200)->assertJson(['name' => 'Gratis']);
    }
    public function testUpdate()
    {
        $this->json('PUT', 'api/tenantsubscriptionplan/1', $this->tenansubscription)->assertStatus(200);
        $this->assertDatabaseHas('tenantSubscriptionPlan', $this->tenansubscription);
    }
    public function testDestroy()
    {
        $this->delete('api/tenantsubscriptionplan/1')->assertStatus(200);
        $this->assertDatabaseMissing('tenantSubscriptionPlan', ['name' => 'premiun']);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM tenantSubscriptionPlan;');
        DB::statement('ALTER TABLE tenantSubscriptionPlan auto_increment=1;');
    }

}
