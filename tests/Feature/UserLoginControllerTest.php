<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\UserLogin;
use App\UserLoginType;

class UserLoginControllerTest extends TestCase
{
    private $user;
    /**
     * 
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(UserLoginType::class)->create();
        factory(UserLogin::class)->create(['userName' => 'fernnx2']);
        factory(UserLogin::class)->create(['userName' => 'devfernando']);
        $this->user = [
            'userLoginTypeId' => 1,
            'contactId' => null,
            'userName' => 'marthy',
            'email' => 'marthy@gmail.com',
            'password' => bcrypt("marthy1234"),
            'password2' => bcrypt("marthy2"),
            'iterations' => 1,
            'confirmKey' => bcrypt("key1234"),
            'secretQuestion' => 'Cual es mi nombre',
            'secretAnswer' => 'marta',
            'offset' => 1,
            'lastLoginDateTime' => null,
            'lastPasswordReset' => null,
            'isLocked' => 1,
            'active' => 1,
            'tenantId' => 1,
            'updatedById' => 1,
            'createdById' => 1,
            'createdPortalBillingLink' => null,
            'portalBillingLink' => null,
            'external_userId' => null,
            'tourIsEnded' => null,
            'isGuest' => 1
        ];
    }

    public function testIndex()
    {
        $this->get('api/userlogin')->assertJsonCount(2)->assertStatus(200);
    }

    public function testStore()
    {
        $this->json('POST', 'api/userlogin', $this->user)->assertStatus(200);
        $this->assertDatabaseHas('userLogin', $this->user);
    }

    public function testShow()
    {
        $this->get('api/userlogin/1')->assertStatus(200)->assertJson(['userName' => 'fernnx2']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/userlogin/2', $this->user)->assertStatus(200);
        $this->assertDatabaseHas('userLogin', $this->user);
    }

    public function testDestroy()
    {
        $this->delete('api/userlogin/1')->assertStatus(200);
        $this->assertDatabaseMissing('userLogin', ['userName' => 'fernnx2']);
    }

    public function tearDown()
    {
        DB::statement(' DELETE FROM userLogin;');
        DB::statement(' ALTER TABLE userLogin auto_increment = 1;');
    }
}
