<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfirmEmail;
use App\Http\Util\ValidatorController;

class GenConfirmEmailController extends Controller
{

    private $rules = [
        'email' => 'required',
        'confirmKey' => 'required',
        'isConfirmed' => 'required',
        'active' => 'required'

    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $confirmEmail = GenConfirmEmail::all();
            return response()->json($confirmEmail, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['created' => false, 'errors' => $validator->errors()->all()], 401);
        }
        try {
            $data = \json_decode($request->getContent(), true);
            $confirm = new GenConfirmEmail();
            $confirm->email = $data['email'];
            $confirm->confirmKey = $data['confirmKey'];
            $confirm->isConfirmed = $data['isConfirmed'];
            $confirm->dateConfirmed = now();
            $confirm->deathline = $data['deathline'];
            $confirm->active = $data['active'];
            $confirm->save();

            return response()->json(["created" => "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $confirmEmail = GenConfirmEmail::where('email', $id)->firstOrFail();
            return response()->json($confirmEmail, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $confirm = GenConfirmEmail::findOrFail($id);
        $validator = \Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['created' => false, 'errors' => $validator->errors()->all()], 401);
        }
        try {
            $data = \json_decode($request->getContent(), true);

            $confirm->email = $data['email'];
            $confirm->confirmKey = $data['confirmKey'];
            $confirm->isConfirmed = $data['isConfirmed'];
            $confirm->dateConfirmed = now();
            $confirm->deathline = $data['deathline'];
            $confirm->active = $data['active'];
            $confirm->save();

            return response()->json(["created" => "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $confirmEmail = GenConfirmEmail::findOrFail($id);
            $confirmEmail->delete();
            return response()->json($confirmEmail, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }
}
