@extends('layout.layout')

@section('content')
<h1>Listing</h1>

<ol>
    <li>{{$property->internalStatus}}</li>
    <li>{{$property->directions}}</li>
    <li>{{$property->area}}</li>
    <li>{{$property->listingPrice}}</li>
    @foreach($owners as $owner )
    <li>{{$owner->firstName}}</li>
    @endforeach
    @foreach($agents as $agent )
    <li>{{$agent->firstName}}</li>
    @endforeach
</ol>

@endsection