<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\TenantSource;

class TenantSourceControllerTest extends TestCase
{
    public $tenantSource = [
        'name' => 'test',
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1
    ];
    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(TenantSource::class)->create(['name' => 'source']);
        factory(TenantSource::class, 5)->create();
    }

    public function testIndex()
    {
        $this->get('api/tenantsource')->assertStatus(200)->assertJsonCount(6);
    }

    public function testStore()
    {
        $this->json('POST', 'api/tenantsource', $this->tenantSource)->assertStatus(200);
        $this->assertDatabaseHas('tenantSource', $this->tenantSource);
    }

    public function testShow()
    {
        $this->get('api/tenantsource/1')->assertStatus(200)->assertJson(['name' => 'source']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/tenantsource/1', $this->tenantSource)->assertStatus(200);
        $this->assertDatabaseHas('tenantSource', $this->tenantSource);
    }

    public function testDestroy()
    {
        $this->delete('api/tenantsource/1')->assertStatus(200);
        $this->assertDatabaseMissing('tenantSource', ['name' => 'source']);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM tenantSource;');
        DB::statement('ALTER TABLE tenantSource auto_increment=1;');
    }

}
