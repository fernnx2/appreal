<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoginRoleXref extends Model
{
    protected $table = 'userLoginRoleXref';
    protected $primaryKey = 'userLoginRoleXrefId';
    public $timestamps = false;
}
