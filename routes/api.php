<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
 *  return $request->user();
});*/

Route::resource(
    'demo',
    'Security\UserRequestDemoController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'employeetype',
    'Setings\EmployeeTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'employeeregister',
    'Setings\EmployeeRegisterController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'country',
    'General\GenCountryController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'currency',
    'General\GenCurrencyController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'language',
    'General\GenLanguageController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'region',
    'General\GenRegionController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'settings',
    'General\GenSettingsController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'timezone',
    'General\GenTimezoneController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'userlogintype',
    'Security\UserLoginTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'userlogin',
    'Security\UserLoginController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'tenantbillingtype',
    'Security\TenantBillingTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'tenantsource',
    'Security\TenantSourceController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'tenantstatus',
    'Security\TenantStatusController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'tenantsubscriptionplan',
    'Security\TenantSubscriptionPlanController',
    ['except' => ['create', 'edit']]
);

Route::post('tenant/images/{id}', 'Security\TenantController@uploadLogo');
Route::resource(
    'tenant',
    'Security\TenantController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'mail',
    'SendEmailController',
    ['only' => ['show']]
);
Route::resource(
    'mail/verify',
    'VerifyEmailController',
    ['only' => ['show']]
);
Route::resource(
    'userloginrole',
    'Security\UserLoginRoleController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'confirmemail',
    'General\GenConfirmEmailController',
    ['except' => ['create', 'edit']]
);

Route::resource(
    'tenantmodule',
    'Security\TenantModuleController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'userloginrolexref',
    'Security\UserLoginRoleXrefController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'appform',
    'Security\AppFormController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'appformoption',
    'Security\AppFormOptionController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'userloginpermission',
    'Security\UserLoginPermissionController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'contact',
    'Customers\ContactController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'config',
    'General\GenConfigController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'configname',
    'General\GenConfigNameController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'confignamegroup',
    'General\GenConfigNameGroupController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'confignamesubgroup',
    'General\GenConfigNameSubGroupController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'confignamevaluetype',
    'General\GenConfigNameValueTypeController',
    ['except' => ['create', 'edit']]
);

Route::get('agent/findbytenant/{id}', 'Listings\LisAgentController@findByTenant');

Route::resource(
    'agent',
    'Listings\LisAgentController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'agenttype',
    'Listings\LisAgentTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'contactphone',
    'Customers\ContactPhoneController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'contactphonetype',
    'Customers\ContactPhoneTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'contacttype',
    'Customers\ContactTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'agentcontact',
    'Listings\LisAgentContactXrefController',
    ['except' => ['create', 'edit', 'index']]
);
Route::resource(
    'ownercontact',
    'Listings\LisOwnerContactXrefController',
    ['except' => ['create', 'edit', 'index']]
);
Route::resource(
    'officetype',
    'Listings\LisOfficeTypeController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'states',
    'Listings\LisStateController',
    ['except' => ['create', 'edit']]
);
Route::get('offices/findbytenant/{id}', 'Listings\LisOfficeController@findByTenant');
Route::resource(
    'offices',
    'Listings\LisOfficeController',
    ['except' => ['create', 'edit']]
);
Route::get('owner/findbytenant/{id}', 'Listings\LisOwnerController@findByTenant');
Route::resource(
    'owner',
    'Listings\LisOwnerController',
    ['except' => ['create', 'edit']]
);
Route::get('officehasagents/agents/{id}', 'Listings\LisOfficehasAgentsController@findAgenthasOffice');
Route::resource(
    'officehasagents',
    'Listings\LisOfficehasAgentsController',
    ['except' => ['create', 'edit']]
);
Route::get('propertykey/find/{id}', 'Listings\LisKeyController@findById');
Route::get('propertykey/findbytenant/{id}', 'Listings\LisKeyController@findByTenant');
Route::get('propertykey/history/{id}', 'Listings\LisKeyController@findKeyHistory');
Route::resource(
    'propertykey',
    'Listings\LisKeyController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'recoverpass',
    'UserMail\UserLoginRecoverPassController',
    ['only' => ['show']]
);
Route::resource(
    'failemailexist',
    'UserMail\UserFailEmailExistController',
    ['only' => ['show']]
);

Route::get('property/tenant/{id}', 'Listings\LisPropertyController@findByTenant');
Route::resource(
    'property',
    'Listings\LisPropertyController',
    ['except' => ['create', 'edit']]
);
Route::resource(
    'propertyroms',
    'Listings\LisRomhasPropertyController',
    ['except' => ['create', 'edit']]
);
Route::get('agentpropertys/agents/{id}', 'Listings\LisAgenthasPropertyController@findByProperty');
Route::resource(
    'agentpropertys',
    'Listings\LisAgenthasPropertyController',
    ['except' => ['create', 'edit']]
);

Route::get('ownerpropertys/owner/{id}', 'Listings\LisOwnerhasPropertyController@findByProperty');
Route::resource(
    'ownerpropertys',
    'Listings\LisOwnerhasPropertyController',
    ['except' => ['create', 'edit']]
);

Route::resource(
    'roms',
    'Listings\LisRomsController',
    ['except' => ['create', 'edit']]
);

Route::get('appoinments/tenant/{id}', 'Listings\LisAppoinmentsController@findByTenantId');
Route::resource(
    'appoinments',
    'Listings\LisAppoinmentsController',
    ['except' => ['create', 'edit']]
);

Route::get('call/tenant/{id}', 'Listings\LisCallController@findByTenantId');
Route::get('call/filter/tenant/{id}', 'Listings\LisCallController@findByTenantLeadFilter');
Route::resource(
    'call',
    'Listings\LisCallController',
    ['except' => ['create', 'edit']]
);

Route::get('caller/tenant/{id}', 'Listings\LisCallerController@findByTenantId');
Route::resource(
    'caller',
    'Listings\LisCallerController',
    ['except' => ['create', 'edit']]
);

Route::get('categorysourcecontact/source/{id}', 'Listings\LisCategorySourceContactController@findBySourceId');
Route::resource(
    'categorysourcecontact',
    'Listings\LisCategorySourceContactController',
    ['except' => ['create', 'edit']]
);

Route::get('contactmethod/tenant/{id}', 'Listings\LisContactMethodController@findByTenantId');
Route::resource(
    'contactmethod',
    'Listings\LisContactMethodController',
    ['except' => ['create', 'edit']]
);

Route::get('newlisting/tenant/{id}', 'Listings\LisNewListingController@findByTenantId');
Route::get('newlisting/filter/tenant/{id}', 'Listings\LisNewListingController@findByTenantNewListing');
Route::resource(
    'newlisting',
    'Listings\LisNewListingController',
    ['except' => ['create', 'edit']]
);

Route::get('sourcecontact/tenant/{id}', 'Listings\LisSourceContactController@findByTenantId');
Route::resource(
    'sourcecontact',
    'Listings\LisSourceContactController',
    ['except' => ['create', 'edit']]
);

Route::get('status/tenant/{id}', 'Listings\LisStatusController@findByTenantId');
Route::resource(
    'status',
    'Listings\LisStatusController',
    ['except' => ['create', 'edit']]
);

Route::get('typeappoinments/tenant/{id}', 'Listings\LisTypeAppoinmentsController@findByTenantId');
Route::resource(
    'typeappoinments',
    'Listings\LisTypeAppoinmentsController',
    ['except' => ['create', 'edit']]
);

Route::resource(
    'changepassword',
    'UserMail\UserLoginChangePassController',
    ['only' => ['show']]
);

//Rutas para control de imagenes para la propiedad
Route::post('property/images/upload', 'Image\StorageController@save');
Route::get('property/images/{id}', 'Image\StorageController@findByProperty');
Route::delete('property/images/{id}', 'Image\StorageController@delete');

Route::post('agent/images/{id}', 'Listings\LisAgentController@uploadPhoto');
Route::delete('agent/images/{id}', 'Listings\LisAgentController@deletePhoto');

//ruteo para reportes

Route::get('agent/pdf/{id}', 'Listings\LisAgentController@pdf');
Route::get('property/pdf/{id}', 'Listings\LisPropertyController@pdf');
Route::get('call/pdf/{id}', 'Listings\LisCallController@pdf');
Route::get('appoinment/pdf/{id}', 'Listings\LisAppoinmentsController@pdf');