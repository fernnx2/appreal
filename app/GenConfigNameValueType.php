<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfigNameValueType extends Model
{
    protected $table = 'genConfigNameValueType';
    protected $primaryKey = 'genConfigNameValueTypeId';
    public $timestamps = false;
}
