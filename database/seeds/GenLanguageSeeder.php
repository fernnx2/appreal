<?php

use Illuminate\Database\Seeder;

class GenLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genLanguage')->insert([
            'code' => '1',
            'name' => 'Español',
            'active' => 1,
            'culturename' => 'Latinos',
            'cultureid' => '1',
            'createdById' => '1',
            'updatedById' => '1',
            'insertDateTime' => now(),
        ]);
        DB::table('genLanguage')->insert([
            'code' => '1',
            'name' => 'Ingles',
            'active' => 1,
            'culturename' => 'Gringos',
            'cultureid' => '2',
            'createdById' => '1',
            'updatedById' => '1',
            'insertDateTime' => now(),
        ]);
    }
}
