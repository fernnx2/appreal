<?php

use Faker\Generator as Faker;
use App\EmployeeType;

$factory->define(EmployeeType::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
