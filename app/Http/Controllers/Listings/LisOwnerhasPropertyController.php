<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LisOwnerhasProperty;
use App\Http\Util\ValidatorController;

class LisOwnerhasPropertyController extends Controller
{
    private $rules = [
        'ownerId' => 'required',
        'propertyId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $ownerProperty = new LisOwnerhasProperty();
            $ownerProperty->ownerId = $data['ownerId'];
            $ownerProperty->propertyId = $data['propertyId'];
            $ownerProperty->updatedById = $data['updatedById'];
            $ownerProperty->createdById = $data['createdById'];
            $ownerProperty->save();
            return response()->json($ownerProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => 'Fail'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $agentProperty = LisOwnerhasProperty::where('ownerId', $id)->get();
            return response()->json($agentProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByAgentId' => 'Fail'], 400);
        }
    }

    public function findByProperty($id)
    {

        try {
            $ownerProperty = LisOwnerhasProperty::where('propertyId', $id)->get();
            $owners = DB::table('lisOwnerhasProperty')
                ->join('lisOwner', 'lisOwner.lisOwnerId', '=', 'lisOwnerhasProperty.ownerId')
                ->select('lisOwner.*')
                ->where('lisOwnerhasProperty.propertyId', '=', $id)
                ->get();

            for ($i = 0; $i < \sizeof($ownerProperty); $i++) {
                for ($o = 0; $o < \sizeof($owners); $o++) {
                    $id = $owners[$o]->lisOwnerId;
                    if ($ownerProperty[$i]['ownerId'] === $id) {
                        $ownerProperty[$i]['ownerId'] = $owners;
                    }
                }
            }
            return response()->json($ownerProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByAgentId' => $th], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ownerProperty = LisOwnerhasProperty::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $ownerProperty->ownerId = $data['ownerId'];
            $ownerProperty->propertyId = $data['propertyId'];
            $ownerProperty->updatedById = $data['updatedById'];
            $ownerProperty->createdById = $data['createdById'];
            $ownerProperty->modifyDateTime = now();
            $ownerProperty->save();
            return response()->json($ownerProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => 'Fail'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ownerProperty = LisOwnerhasProperty::findOrFail($id);
            $ownerProperty->delete();
            return response()->json(['Delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }
}
