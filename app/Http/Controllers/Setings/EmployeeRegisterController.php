<?php

namespace App\Http\Controllers\Setings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmployeeRegister;
use App\Http\Util\ValidatorController;

class EmployeeRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $employeeRegister = EmployeeRegister::all();
            return response()->json($employeeRegister, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $data = \json_decode($request->getContent(), true);
            for ($i = 0; $i < sizeof($data); $i++) {
                $employeeRegister = new EmployeeRegister();
                $employeeRegister->nombre = $data[$i]['nombre'];
                $employeeRegister->apellido = $data[$i]['apellido'];
                $employeeRegister->usuario = $data[$i]['usuario'];
                $employeeRegister->contraseña = $data[$i]['contraseña'];
                $employeeRegister->correoElectronico = $data[$i]['correoElectronico'];
                $employeeRegister->rol = $data[$i]['rol'];
                $employeeRegister->updatedById = $data[$i]['updatedById'];
                $employeeRegister->createdById = $data[$i]['createdById'];
                $employeeRegister->save();

            }
            return response()->json(["create" => "Exito!"], 200);
        } catch (\PDOException $e) {

            return response()->json(["create" => $e], 400);
        }

    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employeeRegister = UserRequestDemo::findOrFail($id);
        try {
            return response()->json($employeeRegister, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findById" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = \json_decode($request->getContent(), true);
            for ($i = 0; $i < sizeof($data); $i++) {
                $employeeRegister = EmployeeRegister::findOrFail($id);
                $employeeRegister->nombre = $data[$i]['nombre'];
                $employeeRegister->apellido = $data[$i]['apellido'];
                $employeeRegister->usuario = $data[$i]['usuario'];
                $employeeRegister->contraseña = $data[$i]['contraseña'];
                $employeeRegister->correoElectronico = $data[$i]['correoElectronico'];
                $employeeRegister->rol = $data[$i]['rol'];
                $employeeRegister->updatedById = $data[$i]['updatedById'];
                $employeeRegister->createdById = $data[$i]['createdById'];
                $employeeRegister->modifyDateTime = now();
                $employeeRegister->save();
            }
            return response()->json(["update" => "Exito!"], 200);
        } catch (\PDOException $e) {

            return response()->json(["update" => $e], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employeeRegister = UserRequestDemo::findOrFail($id);
        try {
            $employeeRegister->delete();
            return response()->json(["delete" => "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
