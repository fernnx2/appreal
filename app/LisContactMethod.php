<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisContactMethod extends Model
{
    protected $table = 'lisContactMethod';
    protected $primaryKey = 'lisContactMethodId';
    public $timestamps = false;
}
