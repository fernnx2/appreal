<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AppFormOption;
use App\Http\Util\ValidatorController;

class AppFormOptionController extends Controller
{
    private $rules = [
        'objectName' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $app = AppFormOption::all();
            return response()->json($app, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $app = new AppFormOption();
            $app->appOptionTypeId = $data['appOptionTypeId'];
            $app->objectName = $data['objectName'];
            $app->active = $data['active'];
            $app->updatedById = $data['updatedById'];
            $app->createdById = $data['createdById'];
            $app->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $app = AppFormOption::findOrFail($id);
            return response()->json($app, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $app = AppFormOption::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $app = new AppFormOption();
            $app->appOptionTypeId = $data['appOptionTypeId'];
            $app->objectName = $data['objectName'];
            $app->active = $data['active'];
            $app->updatedById = $data['updatedById'];
            $app->createdById = $data['createdById'];
            $app->modifyDateTime = now();
            $app->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $app = AppFormOption::findOrFail($id);
            $app->delete();
            return response()->json($app, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }
}
