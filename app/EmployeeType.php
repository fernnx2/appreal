<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    protected $table = 'employeeType';
    protected $primaryKey = 'employeeTypeId';
    public $timestamps = false;
}
