<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfigNameSubGroup extends Model
{
    protected $table = 'genConfigNameSubGroup';
    protected $primaryKey = 'genConfigNameSubGroupId';
    public $timestamps = false;
}
