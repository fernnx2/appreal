<?php

use Faker\Generator as Faker;

$factory->define(App\UserLogin::class, function (Faker $faker) {
    return [
        'userLoginTypeId' => 1,
        'contactId' => null,
        'userName' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'password2' => bcrypt($faker->password),
        'iterations' => $faker->randomDigit,
        'confirmKey' => bcrypt($faker->password),
        'secretQuestion' => $faker->randomLetter,
        'secretAnswer' => $faker->randomLetter,
        'offset' => $faker->randomDigit,
        'lastLoginDateTime' => $faker->dateTime,
        'lastPasswordReset' => $faker->dateTime,
        'isLocked' => 1,
        'active' => 1,
        'tenantId' => $faker->randomDigit,
        'updatedById' => $faker->randomDigit,
        'createdById' => $faker->randomDigit,
        'createdPortalBillingLink' => $faker->dateTime,
        'portalBillingLink' => $faker->text,
        'external_userId' => $faker->name,
        'tourIsEnded' => 1,
        'isGuest' => 1
    ];
});
