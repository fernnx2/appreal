<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisKey;
use App\Http\Util\ValidatorController;
use App\LisKeyHistory;

class LisKeyController extends Controller
{
    private $rules = [
        'key' => 'required',
        'nameResponsable' => 'required',
        'tenantId' => 'required',
        'address' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $keys = LisKey::all();
            return response()->json($keys, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $key = new LisKey();
            $key->propertyId = $data['propertyId'];
            $key->tenantId = $data['tenantId'];
            $key->key = $data['key'];
            $key->nameResponsable = $data['nameResponsable'];
            $key->address = $data['address'];
            $key->notes = $data['notes'];
            $key->updatedById = $data['updatedById'];
            $key->createdById = $data['createdById'];
            $key->city = $data['city'];
            $key->statesId = $data['statesId'];
            $key->zipcode = $data['zipcode'];
            $key->status = $data['status'];
            $key->modifyDateTime = now();
            $key->save();
            return response()->json($key, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => $th], 400);
        }
    }

    /**
     * Mustra todas las llaves de una propiedad
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $keys = LisKey::where('propertyId', $id)->get();
            return response()->json($keys, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }

    //busca una key id
    public function findById($id)
    {
        try {
            $keys = LisKey::findOrFail($id);
            return response()->json($keys, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }

    public function findByTenant($id)
    {
        try {
            $keys = LisKey::where('tenantId', $id)->get();
            return response()->json($keys, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function findKeyHistory($id)
    {
        try {
            $keyHistory = LisKeyHistory::where('lisKeyId', $id)->get();
            return response()->json($keyHistory, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findHistory" => "Fail"], 400);
        }
    }

    public function saveHistory(LisKey $key)
    {
        try {
            $keyHistory = new LisKeyHistory();
            $keyHistory->lisKeyId = $key['lisKeyId'];
            $keyHistory->nameResponsable = $key['nameResponsable'];
            $keyHistory->dateStart = $key['modifyDateTime'];
            $keyHistory->dateFinal = now();
            $keyHistory->notes = $key['notes'];
            $keyHistory->save();
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["updated" => "Fail"], 400);
        }
    }


    public function update(Request $request, $id)
    {
        $key = LisKey::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        $this->saveHistory($key);
        try {
            $data = \json_decode($request->getContent(), true);
            $key->propertyId = $data['propertyId'];
            $key->tenantId = $data['tenantId'];
            $key->key = $data['key'];
            $key->nameResponsable = $data['nameResponsable'];
            $key->address = $data['address'];
            $key->notes = $data['notes'];
            $key->updatedById = $data['updatedById'];
            $key->createdById = $data['createdById'];
            $key->modifyDateTime = now();
            $key->city = $data['city'];
            $key->statesId = $data['statesId'];
            $key->zipcode = $data['zipcode'];
            $key->status = $data['status'];
            $key->save();
            return response()->json($key, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["updated" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $keys = LisKey::findOrFail($id);
            $keys->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
