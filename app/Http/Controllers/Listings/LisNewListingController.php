<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LisNewListing;
use App\Http\Util\ValidatorController;

class LisNewListingController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'statusId' => 'required',
        'callId' => 'required'
    ];
    /**
     * 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $listings = LisNewListing::all();
            return response()->json($listings, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $listing = new LisNewListing();
            $listing->tenantId = $data['tenantId'];
            $listing->date = $data['date'];
            $listing->updatedById = $data['updatedById'];
            $listing->createdById = $data['createdById'];
            $listing->statusId = $data['statusId'];
            $listing->callId = $data['callId'];
            $listing->save();
            return response()->json($listing, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $listings = LisNewListing::findOrFail($id);
            return response()->json($listings, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $listings = LisNewListing::where('tenantId', $id)->get();
            return response()->json($listings, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByTenantId' => 'Fail'], 400);
        }
    }

    public function findByTenantNewListing($id)
    {
        try {
            $listings = DB::table('lisNewListing')
                ->join('lisStatus', 'lisStatus.lisStatusId', '=', 'lisNewListing.statusId')
                ->join('lisCall', 'lisCall.lisCallId', '=', 'lisNewListing.callId')
                ->join('lisProperty', 'lisProperty.lisPropertyId', '=', 'lisCall.propertyId')
                ->join('lisAgenthasProperty', 'lisAgenthasProperty.propertyId', '=', 'lisProperty.lisPropertyId')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisAgenthasProperty.agentId')
                ->select(
                    'lisNewListing.*',
                    'lisProperty.mls',
                    'lisProperty.directions',
                    'lisProperty.listingPrice',
                    'lisAgent.firstName',
                    'lisAgent.lastName',
                    'lisStatus.status'
                )
                ->where('lisNewListing.tenantId', $id)->get();
            return response()->json($listings, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByTenantId' => 'Fail'], 400);
        }
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $listing = LisNewListing::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $listing->tenantId = $data['tenantId'];
            $listing->date = $data['date'];
            $listing->updatedById = $data['updatedById'];
            $listing->createdById = $data['createdById'];
            $listing->statusId = $data['statusId'];
            $listing->callId = $data['callId'];
            $listing->modifyDateTime = now();
            $listing->save();
            return response()->json(['update' => 'Exite'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $listings = LisNewListing::findOrFail($id);
            $listings->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
