<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantSource extends Model
{
    protected $table = 'tenantSource';
    protected $primaryKey = 'tenantSourceId';
    public $timestamps = false;
}
