<?php

use Faker\Generator as Faker;

$factory->define(App\UserLoginType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'active' => 1,
        'updatedById' => $faker->randomDigit,
        'createdById' => $faker->randomDigit
    ];
});
