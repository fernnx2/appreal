<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisCall extends Model
{
    protected $table = 'lisCall';
    protected $primaryKey = 'lisCallId';
    public $timestamps = false;
}
