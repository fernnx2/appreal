<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LisProperty;
use App\Http\Util\ValidatorController;
use Barryvdh\DomPDF\Facade as PDF;

class LisPropertyController extends Controller
{
    private $rules = [
        'typePropertyId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required',
        'mls' => 'required',
        'mlsStatus' => 'required',
        'tenantId' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $propertys = LisProperty::all();
            return response()->json($propertys, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $property = new LisProperty();
            $property->typePropertyId = $data['typePropertyId'];
            $property->internalStatus = $data['internalStatus'];
            $property->mls = $data['mls'];
            $property->mlsStatus = $data['mlsStatus'];
            $property->listingPrice = $data['listingPrice'];
            $property->levels = $data['levels'];
            $property->sqft = $data['sqft'];
            $property->baths = $data['baths'];
            $property->remarks = $data['remarks'];
            $property->directions = $data['directions'];
            $property->area = $data['area'];
            $property->updatedById = $data['updatedById'];
            $property->createdById = $data['createdById'];
            $property->houseNumber = $data['houseNumber'];
            $property->streetDir = $data['streetDir'];
            $property->streetName = $data['streetName'];
            $property->unitNumber = $data['unitNumber'];
            $property->style = $data['style'];
            $property->bedrooms = $data['bedrooms'];
            $property->fireplace = $data['fireplace'];
            $property->garage = $data['garage'];
            $property->squareFeet = $data['squareFeet'];
            $property->yearBuild = $data['yearBuild'];
            $property->basement = $data['basement'];
            $property->siding = $data['siding'];
            $property->lotSize = $data['lotSize'];
            $property->tazes = $data['tazes'];
            $property->legalDescription = $data['legalDescription'];
            $property->zoning = $data['zoning'];
            $property->lisDate = $data['lisDate'];
            $property->expDate = $data['expDate'];
            $property->adCode = $data['adCode'];
            $property->keyCode = $data['keyCode'];
            $property->singNumber = $data['singNumber'];
            $property->notes = $data['notes'];
            $property->loanType = $data['loanType'];
            $property->balace = $data['balace'];
            $property->lender = $data['lender'];
            $property->terms = $data['terms'];
            $property->district = $data['district'];
            $property->elementary = $data['elementary'];
            $property->middleJrHigh = $data['middleJrHigh'];
            $property->high = $data['high'];
            $property->tenantId = $data['tenantId'];
            $property->city = $data['city'];
            $property->statesId = $data['statesId'];
            $property->zipcode = $data['zipcode'];
            $property->save();
            return response()->json($property, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $property = LisProperty::findOrFail($id);
            return response()->json($property, 200);
        } catch (\PDOException $th) {
            return response()->json(['Find' => 'Fail'], 400);
        }
    }

    public function findByTenant($id)
    {
        try {
            $property = LisProperty::where('tenantId', $id)->get();
            return response()->json($property, 200);
        } catch (\PDOException $th) {
            return response()->json(['Find' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $property = LisProperty::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $property->typePropertyId = $data['typePropertyId'];
            $property->internalStatus = $data['internalStatus'];
            $property->mls = $data['mls'];
            $property->mlsStatus = $data['mlsStatus'];
            $property->listingPrice = $data['listingPrice'];
            $property->levels = $data['levels'];
            $property->sqft = $data['sqft'];
            $property->baths = $data['baths'];
            $property->remarks = $data['remarks'];
            $property->directions = $data['directions'];
            $property->area = $data['area'];
            $property->updatedById = $data['updatedById'];
            $property->createdById = $data['createdById'];
            $property->modifyDateTime = now();
            $property->houseNumber = $data['houseNumber'];
            $property->streetDir = $data['streetDir'];
            $property->streetName = $data['streetName'];
            $property->unitNumber = $data['unitNumber'];
            $property->style = $data['style'];
            $property->bedrooms = $data['bedrooms'];
            $property->fireplace = $data['fireplace'];
            $property->garage = $data['garage'];
            $property->squareFeet = $data['squareFeet'];
            $property->yearBuild = $data['yearBuild'];
            $property->basement = $data['basement'];
            $property->siding = $data['siding'];
            $property->lotSize = $data['lotSize'];
            $property->tazes = $data['tazes'];
            $property->legalDescription = $data['legalDescription'];
            $property->zoning = $data['zoning'];
            $property->lisDate = $data['lisDate'];
            $property->expDate = $data['expDate'];
            $property->adCode = $data['adCode'];
            $property->keyCode = $data['keyCode'];
            $property->singNumber = $data['singNumber'];
            $property->notes = $data['notes'];
            $property->loanType = $data['loanType'];
            $property->balace = $data['balace'];
            $property->lender = $data['lender'];
            $property->terms = $data['terms'];
            $property->district = $data['district'];
            $property->elementary = $data['elementary'];
            $property->middleJrHigh = $data['middleJrHigh'];
            $property->high = $data['high'];
            $property->tenantId = $data['tenantId'];
            $property->city = $data['city'];
            $property->statesId = $data['statesId'];
            $property->zipcode = $data['zipcode'];
            $property->save();
            return response()->json(['Update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $property = LisProperty::findOrFail($id);
            $property->delete();
            return response()->json(['Delete' => 'Fail'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }

    public function pdf($id)
    {
        try {
            $property = LisProperty::findOrFail($id);
            $owners = DB::table('lisOwnerhasProperty')
                ->join('lisOwner', 'lisOwner.lisOwnerId', '=', 'lisOwnerhasProperty.ownerId')
                ->select('lisOwner.*')
                ->where('lisOwnerhasProperty.propertyId', '=', $id)->get();
            $agents = DB::table('lisAgenthasProperty')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisAgenthasProperty.agentId')
                ->select('lisAgent.*')
                ->where('lisAgenthasProperty.propertyId', '=', $id)
                ->get();
            $pdf = PDF::loadView('Reportes.listing', compact('property', 'owners', 'agents'));
            return $pdf->stream('listing.pdf');
        } catch (\PDOException $th) {
            return response()->json(['Find' => 'Fail'], 400);
        }
    }
}
