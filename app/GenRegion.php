<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenRegion extends Model
{
    protected $table = 'genRegion';
    protected $primaryKey = 'genRegionId';
    public $timestamps = false;
}
