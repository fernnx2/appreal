<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TenantSubscriptionPlan;
use App\Http\Util\ValidatorController;

class TenantSubscriptionPlanController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tenantsubscription = TenantSubscriptionPlan::all();
            return response()->json($tenantsubscription, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantsubscription = new TenantSubscriptionPlan();
            $tenantsubscription->code = $data['code'];
            $tenantsubscription->name = $data['name'];
            $tenantsubscription->active = $data['active'];
            $tenantsubscription->durationDays = $data['durationDays'];
            $tenantsubscription->updatedById = $data['updatedById'];
            $tenantsubscription->createdById = $data['createdById'];
            $tenantsubscription->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tenantsubscriptionplan = TenantSubscriptionPlan::findOrFail($id);
            return response()->json($tenantsubscriptionplan, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenantsubscription = TenantSubscriptionPlan::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantsubscription->code = $data['code'];
            $tenantsubscription->name = $data['name'];
            $tenantsubscription->active = $data['active'];
            $tenantsubscription->durationDays = $data['durationDays'];
            $tenantsubscription->updatedById = $data['updatedById'];
            $tenantsubscription->createdById = $data['createdById'];
            $tenantsubscription->modifyDateTime = now();
            $tenantsubscription->save();
            return response()->json('update', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tenantsubscription = TenantSubscriptionPlan::findOrFail($id);
            $tenantsubscription->delete();
            return response()->json('delete', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
