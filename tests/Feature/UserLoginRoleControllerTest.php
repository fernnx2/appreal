<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\UserLoginRole;

class UserLoginRoleControllerTest extends TestCase
{
    /**
     * 
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        //poblando de registros la bd
        factory(UserLoginRole::class)->create([
            'name' => 'admin'
        ]);
        factory(UserLoginRole::class)->create([
            'name' => 'owner'
        ]);
        factory(UserLoginRole::class, 10)->create();

    }

    public function testIndex()
    {
        $this->get('api/userloginrole')->assertJsonCount(12)->assertStatus(200);
    }

    public function testStore()
    {
        $this->json('POST', 'api/userloginrole', [
            'name' => 'fer',
            'code' => 'rr',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200);
        $this->assertDatabaseHas('userLoginRole', [
            'name' => 'fer',
            'code' => 'rr',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ]);
    }

    public function testShow()
    {
        $this->get('api/userloginrole/1')->assertJson(['name' => 'admin'])->assertStatus(200);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/userloginrole/2', [
            'name' => 'superuser',
            'code' => '1',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200);
        $this->assertDatabaseHas('userLoginRole', [
            'name' => 'superuser',
            'code' => '1',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ]);
    }

    public function testDestroy()
    {
        $this->delete('api/userloginrole/1')->assertStatus(200);
        $this->assertDatabaseMissing('userLoginRole', ['name' => 'admin']);
    }

    public function tearDown()
    {
        DB::statement(' DELETE FROM userLoginRole;');
        DB::statement(' ALTER TABLE userLoginRole auto_increment = 1;');
    }

}
