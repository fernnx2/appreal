<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\UserRequestDemo;

class UserRequestDemoSendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $userRequestDemoConfirm;
    public function __construct(UserRequestDemo $userRequestDemoConfirm)
    {
        $this->userRequestDemoConfirm = $userRequestDemoConfirm;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $address = 'devfernando95@gmail.com';
        $subject = 'Mensaje de Confirmacion!';
        $name = 'RealDeal';

        return $this->view('mails.userRequestDemoConfirm_call')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with(['message' => $this->userRequestDemoConfirm]);
    }
}
