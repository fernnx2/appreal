<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
    protected $table = 'contactType';
    protected $primaryKey = 'contactTypeId';
    public $timestamps = false;
}
