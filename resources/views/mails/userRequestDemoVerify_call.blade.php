<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Subscription Successful</title>
</head>
<body>
  
  <div style="padding:0;margin:0 auto;font-size:14px;font-family:arial">

    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0 auto;padding:0;max-width:612px" class="m_-161757149103433228container" align="center">
      <tbody>

        <tr>
          <td style="padding:30px 0 20px 20px">Dear {{$userRequestDemoVerify->firstName}}:</td>
        </tr>
        <tr>
          <td style="padding:0 20px 20px"><strong>Welcome to RealDeal.</strong></td>
        </tr>
        <tr>
          <td style="padding:0 20px 20px">Below you will find a bonton that will carry your account.</td>
        </tr>
        <tr>
          <td style="padding:0">
            <table border="0" cellpadding="0" width="100%" cellspacing="0" style="margin:0;padding:0;max-width:612px;border:1px solid #edf1f1;color:#5f5f5f">

              <tbody>
                <tr style="background:#edf1f1">
                  <td style="text-align:center;vertical-align:top;font-size:0;padding:15px 0">

                    <div style="width:100%;vertical-align:top">

                      <table width="50%" align="center">
                        <tbody>
                          <tr>
                            <td align="center" style="background:#5d9cec;padding:13px 0">
                              <a href="http://68.183.156.229/login" style="text-decoration:none;color:#fff;font-size:20px;font-weight:bold;display:block;text-align:center"><strong style="text-decoration:none;color:#fff">Start my account in RealDeal</strong></a>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                    </div>
                  </td>
                </tr>

              </tbody>
            </table>
          </td>
        </tr>

        <tr>
          <td style="padding:20px 20px 0">We sent you this communication because recently an account with your email address was created. If you have not created this account, send a request for help to the email to <a href="#"
              target="_blank"> soporte@realapp.com</a>.</td>
        </tr>

        <tr>
          <td style="padding:16px 20px 25px">
            Thank you, <br> The account team of RealDeal.
          </td>
        </tr>

        <tr>
          <td style="padding:10px 20px 0" class="m_-161757149103433228nopadding">
            <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;width:100%" class="m_-161757149103433228footer-wrap">
              <tbody>
                <tr>
                  <td style="width:45%;font-size:10px;padding-bottom:10px" class="m_-161757149103433228full-width-center">
                    Copyright @ 2018, RealDeal. <br> All rights reserved.
                  </td>

                </tr>
              </tbody>
            </table>
          </td>
        </tr>

      </tbody>
    </table>
  </div>
</body>
</html>


