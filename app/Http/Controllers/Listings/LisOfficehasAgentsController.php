<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Util\ValidatorController;
use App\LisOfficehasAgents;

class LisOfficehasAgentsController extends Controller
{
    private $rules = [
        'agentId' => 'required',
        'officeId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $officeAgent = new LisOfficehasAgents();
            $officeAgent->officeId = $data['officeId'];
            $officeAgent->agentId = $data['agentId'];
            $officeAgent->updatedById = $data['updatedById'];
            $officeAgent->createdById = $data['createdById'];
            $officeAgent->save();
            return response()->json($officeAgent, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $officeAgent = DB::table('lisOfficehasAgents')
                ->join('lisOffice', 'lisOffice.lisofficeId', '=', 'lisOfficehasAgents.officeId')
                ->select('lisOffice.*', 'lisOfficehasAgents.lisOfficehasAgentsId')
                ->where('lisOfficehasAgents.agentId', '=', $id)
                ->get();
            return response()->json($officeAgent, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }

    public function findAgenthasOffice($id)
    {
        try {
            $officeAgent = DB::table('lisOfficehasAgents')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisOfficehasAgents.agentId')
                ->select('lisAgent.*', 'lisOfficehasAgents.lisOfficehasAgentsId')
                ->where('lisOfficehasAgents.officeId', '=', $id)
                ->get();
            return response()->json($officeAgent, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $officeAgent = LisOfficehasAgents::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $officeAgent->officeId = $data['officeId'];
            $officeAgent->agentId = $data['agentId'];
            $officeAgent->updatedById = $data['updatedById'];
            $officeAgent->createdById = $data['createdById'];
            $officeAgent->modifyDateTime = now();
            $officeAgent->save();
            return response()->json($officeAgent, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $officeAgent = LisOfficehasAgents::findOrFail($id);
            $officeAgent->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => $th], 400);
        }
    }
}
