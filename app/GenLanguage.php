<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenLanguage extends Model
{
    protected $table = 'genLanguage';
    protected $primaryKey = 'genLanguageId';
    public $timestamps = false;
}
