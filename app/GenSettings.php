<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenSettings extends Model
{
    protected $table = 'genSettings';
    protected $primaryKey = 'genSettingsId';
    public $timestamps = false;
}
