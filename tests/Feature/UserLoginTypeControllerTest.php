<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\UserLoginType;

class UserLoginTypeControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        //poblando de registros la bd
        factory(UserLoginType::class)->create([
            'name' => 'admin'
        ]);
        factory(UserLoginType::class)->create([
            'name' => 'user'
        ]);
        factory(UserLoginType::class, 10)->create();

    }

    public function testIndex()
    {
        $this->get('api/userlogintype')->assertJsonCount(12)->assertStatus(200);
    }
    public function testStore()
    {
        $this->json('POST', 'api/userlogintype', [
            'name' => 'root',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200);
        $this->assertDatabaseHas('userLoginType', [
            'name' => 'root',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ]);
    }

    public function testShow()
    {
        $this->get('api/userlogintype/1')->assertJson(['name' => 'admin'])->assertStatus(200);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/userlogintype/2', [
            'name' => 'superuser',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200);
        $this->assertDatabaseHas('userLoginType', [
            'name' => 'superuser',
            'active' => 1,
            'updatedById' => 1,
            'createdById' => 1
        ]);
    }

    public function testDestroy()
    {
        $this->delete('api/userlogintype/1')->assertStatus(200);
        $this->assertDatabaseMissing('userLoginType', ['name' => 'admin']);
    }

    public function tearDown()
    {
        DB::statement(' DELETE FROM userLoginType;');
        DB::statement(' ALTER TABLE userLoginType auto_increment = 1;');
    }
}
