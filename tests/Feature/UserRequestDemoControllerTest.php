<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\UserRequestDemo;
use Illuminate\Support\Facades\DB;

class UserRequestDemoControllerTest extends TestCase
{
    /**
     * Metdo setUp se ejecuta en cada test 
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
    
        //este sera el primer registro con id 1 
        factory(UserRequestDemo::class)->create([
            'firstName' => 'fernando',
            'lastName' => 'menjivar',
            'company' => 'devfernando.ga',
            'email' => 'email@gmail.com',
            'phone' => '77343577',
            'employees' => 8,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ]);
        //este sera el segundo registro con id 2
        factory(UserRequestDemo::class)->create([
            'firstName' => 'marta',
            'lastName' => 'portillo',
            'company' => 'devmarthy.ga',
            'email' => 'marthy@gmail.com',
            'phone' => '77343577',
            'employees' => 8,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ]);

    }

    public function testIndex()
    {
        $this->json('GET', 'api/demo')->assertJsonCount(2)->assertStatus(200);

    }

    public function testStore()
    {
        $this->json('POST', 'api/demo', [
            'firstName' => 'rene',
            'lastName' => 'aguilera',
            'company' => 'devrene',
            'email' => 'rene@gmail.com',
            'phone' => '77343577',
            'employees' => 8,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200)->assertJson(['userRequestDemoId' => 3]);
        $this->assertDatabaseHas('userRequestDemo', [
            'firstName' => 'rene',
            'lastName' => 'aguilera',
            'company' => 'devrene',
            'email' => 'rene@gmail.com',
            'phone' => '77343577',
            'employees' => 8,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ]);

    }

    public function testShow()
    {
        $this->json('GET', 'api/demo/1')->assertJson([
            'firstName' => 'fernando',
            'lastName' => 'menjivar',
            'company' => 'devfernando.ga',
            'email' => 'email@gmail.com',
            'phone' => '77343577',
            'employees' => 8,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200);

    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/demo/1', [
            'firstName' => 'karla',
            'lastName' => 'nuñez',
            'company' => 'karlita',
            'email' => 'karl@gmail.com',
            'phone' => '23232323',
            'employees' => 10,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ])->assertStatus(200);
        $this->assertDatabaseHas('userRequestDemo', [
            'firstName' => 'karla',
            'lastName' => 'nuñez',
            'company' => 'karlita',
            'email' => 'karl@gmail.com',
            'phone' => '23232323',
            'employees' => 10,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ]);
    }

    public function testDestroy()
    {
        $this->delete('api/demo/2')->assertStatus(200);
        $this->assertDatabaseMissing('userRequestDemo', [
            'firstName' => 'marta',
            'lastName' => 'portillo',
            'company' => 'devmarthy.ga',
            'email' => 'marthy@gmail.com',
            'phone' => '77343577',
            'employees' => 8,
            'observaciones' => 'ninguna',
            'updatedById' => 1,
            'createdById' => 1
        ]);

    }

    public function tearDown()
    {
        DB::statement(' DELETE FROM userRequestDemo;');
        DB::statement(' ALTER TABLE userRequestDemo auto_increment = 1;');
    }


}
