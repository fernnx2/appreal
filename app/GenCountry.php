<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenCountry extends Model
{
    protected $table = 'genCountry';
    protected $primaryKey = 'genCountryId';
    public $timestamps = false;
}
