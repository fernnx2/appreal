<?php

use Faker\Generator as Faker;

$factory->define(App\Tenant::class, function (Faker $faker) {
    return [
        'tenantSubscriptionPlanId' => $faker->numberBetween(1, 3),
        'tenantStatusId' => $faker->numberBetween(1, 3),
        'tenantSourceId' => $faker->numberBetween(1, 3),
        'companyName' => $faker->name,
        'companyURL' => $faker->name,
        'database' => $faker->name,
        'allocatedUsers' => $faker->randomDigit,
        'billingRefNumber' => $faker->numberBetween(1, 9),
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1,
        'tenentBillingTypeId' => null
    ];
});
