@extends('layout.layout')

@section('content')
<h1 class="header">Informacion del Agente</h1>
<table>
    <thead>
    <tr>
        <td>firstName</td> 
           <td>lastName</td> 
           <td>mlsid </td>
           <td>miscid</td> 
           <td>fax </td>
           <td>email</td> 
           <td>text</td> 
           <td>webpage </td>
           <td>active</td> 
           <td>address</td> 
           <td>notes</td> 
            </tr> 
    </thead>
    <tbody>
    <tr>
        <td>{{$agent->firstName}}</td> 
           <td>{{$agent->lastName}}</td> 
           <td>{{$agent->mlsid}} </td>
           <td>{{$agent->miscid}}</td> 
           <td>{{$agent->fax}} </td>
           <td>{{$agent->email}}</td> 
           <td>{{$agent->text}}</td> 
           <td>{{$agent->webpage}} </td>
           <td>{{$agent->active}}</td> 
           <td>{{$agent->address}}</td> 
           <td>{{$agent->notes}}</td> 
            </tr> 
    </thead>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <td>MLS #</td> 
           <td>Address</td> 
           <td>Price</td>
           <td>SQ.FT</td> 
           <td>Type</td>
           <td>Status</td> 
            </tr> 
    </thead>
    <tbody>
        @foreach($propertys as $property)
    <tr>
        <td>{{$property->mls}}</td> 
           <td>{{$property->directions}} </td>
           <td>{{$property->listingPrice}}</td> 
           <td>{{$property->typeProperty}} </td>
           <td>{{$property->mlsStatus}}</td> 
           
            </tr> 
        @endforeach
    </thead>
    </tbody>
</table>

@endsection