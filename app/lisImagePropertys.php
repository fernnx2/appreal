<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LisProperty;

class lisImagePropertys extends Model
{
    protected $table = 'lisImagePropertys';
    protected $primaryKey = 'lisImagePropertysId';
    public $timestamps = false;


    public function contactPhoneType()
    {
        return $this->belongsTo(LisProperty::class, 'propertyId');
    }
}
