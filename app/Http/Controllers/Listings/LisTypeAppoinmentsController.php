<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisTypeAppoinment;
use App\Http\Util\ValidatorController;

class LisTypeAppoinmentsController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'type' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $types = LisTypeAppoinment::all();
            return response()->json($types, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $type = new LisTypeAppoinment();
            $type->tenantId = $data['tenantId'];
            $type->type = $data['type'];
            $type->description = $data['description'];
            $type->updatedById = $data['updatedById'];
            $type->createdById = $data['createdById'];
            $type->save();
            return response()->json($type, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $types = LisTypeAppoinment::findOrFail($id);
            return response()->json($types, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 200);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $types = LisTypeAppoinment::where('tenantId', $id)->get();
            return response()->json($types, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByTenantId' => 'Fail'], 200);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = LisTypeAppoinment::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $type->tenantId = $data['tenantId'];
            $type->type = $data['type'];
            $type->description = $data['description'];
            $type->updatedById = $data['updatedById'];
            $type->createdById = $data['createdById'];
            $type->modifyDateTime = now();
            $type->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $types = LisTypeAppoinment::findOrFail($id);
            $types->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 200);
        }
    }
}
