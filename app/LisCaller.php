<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisCaller extends Model
{
    protected $table = 'lisCaller';
    protected $primaryKey = 'lisCallerId';
    public $timestamps = false;
}
