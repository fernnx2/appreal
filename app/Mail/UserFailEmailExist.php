<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\UserRequestDemo;


class UserFailEmailExist extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $userRequestDemo;
    public function __construct(UserRequestDemo $userRequestDemo)
    {
        $this->userRequestDemo = $userRequestDemo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'devfernando95@gmail.com';
        $subject = 'Error de Email';
        $name = 'RealDeal';

        return $this->view('mails.userFailEmailExist_call')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with(['message' => $this->userRequestDemo]);
    }
}
