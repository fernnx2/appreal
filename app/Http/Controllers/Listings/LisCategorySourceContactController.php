<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisCategorySourceContact;
use App\Http\Util\ValidatorController;

class LisCategorySourceContactController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'sourceContactId' => 'required',
        'category' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $categorys = LisCategorySourceContact::all();
            return response()->json($categorys, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $category = new LisCategorySourceContact();
            $category->tenantId = $data['tenantId'];
            $category->category = $data['category'];
            $category->description = $data['description'];
            $category->updatedById = $data['updatedById'];
            $category->createdById = $data['createdById'];
            $category->sourceContactId = $data['sourceContactId'];
            $category->save();
            return response()->json($category, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $categorys = LisCategorySourceContact::findOrFail($id);
            return response()->json($categorys, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    public function findBySourceId($id)
    {
        try {
            $categorys = LisCategorySourceContact::where('sourceContactId', $id)->get();
            return response()->json($categorys, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindBySourceContactId' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = LisCategorySourceContact::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $category->tenantId = $data['tenantId'];
            $category->category = $data['category'];
            $category->description = $data['description'];
            $category->updatedById = $data['updatedById'];
            $category->createdById = $data['createdById'];
            $category->sourceContactId = $data['sourceContactId'];
            $category->modifyDateTime = now();
            $category->save();
            return reponse()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $categorys = LisCategorySourceContact::findOrFail($id);
            $categorys->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
