<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisOwner extends Model
{
    protected $table = 'lisOwner';
    protected $primaryKey = 'lisOwnerId';
    public $timestamps = false;
}
