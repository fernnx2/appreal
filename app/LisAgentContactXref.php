<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisAgentContactXref extends Model
{
    protected $table = 'lisAgentContactXref';
    protected $primaryKey = 'lisAgentContactXrefId';
    public $timestamps = false;

}
