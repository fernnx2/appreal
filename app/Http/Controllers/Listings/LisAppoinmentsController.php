<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LisAppoinments;
use App\Http\Util\ValidatorController;
use Barryvdh\DomPDF\Facade as PDF;

class LisAppoinmentsController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'date' => 'required',
        'scheduledBy' => 'required',
        'scheduledOn' => 'required',
        'duration' => 'required',
        'propertyId' => 'required',
        'typeAppoinmentId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $appoinmmets = LisAppoinments::all();
            return response()->json($appoinmmets, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $appoinment = new LisAppoinments();
            $appoinment->tenantId = $data['tenantId'];
            $appoinment->date = $data['date'];
            $appoinment->scheduledBy = $data['scheduledBy'];
            $appoinment->scheduledOn = $data['scheduledOn'];
            $appoinment->duration = $data['duration'];
            $appoinment->agentId = $data['agentId'];
            $appoinment->callId = $data['callId'];
            $appoinment->notes = $data['notes'];
            $appoinment->updatedById = $data['updatedById'];
            $appoinment->createdById = $data['createdById'];
            $appoinment->typeAppoinmentId = $data['typeAppoinmentId'];
            $appoinment->status = $data['status'];
            $appoinment->save();
            return response()->json($appoinment, 200);
        } catch (\PDOException $th) {
            return response()->json(['created' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $appoinmmets = LisAppoinments::findOrFail($id);
            return response()->json($appoinmmets, 200);
        } catch (\PDOException $th) {
            return response()->json(['findById' => 'Fail'], 400);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $appoinmmets = LisAppoinments::where('tenantId', $id)->get();
            return response()->json($appoinmmets, 200);
        } catch (\PDOException $th) {
            return response()->json(['findByTenantId' => 'Fail'], 400);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $appoinment = LisAppoinments::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $appoinment->tenantId = $data['tenantId'];
            $appoinment->date = $data['date'];
            $appoinment->scheduledBy = $data['scheduledBy'];
            $appoinment->scheduledOn = $data['scheduledOn'];
            $appoinment->duration = $data['duration'];
            $appoinment->agentId = $data['agentId'];
            $appoinment->callId = $data['callId'];
            $appoinment->notes = $data['notes'];
            $appoinment->updatedById = $data['updatedById'];
            $appoinment->createdById = $data['createdById'];
            $appoinment->typeAppoinmentId = $data['typeAppoinmentId'];
            $appoinment->status = $data['status'];
            $appoinment->modifyDateTime = now();
            $appoinment->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $appoinmmets = LisAppoinments::findOrFail($id);
            $appoinmmets->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }

    public function pdf($id)
    {
        try {
            $appoinment = DB::table('lisAppoinments')
                ->join('lisTypeAppoinment', 'lisTypeAppoinment.lisTypeAppoinmentId', '=', 'lisAppoinments.typeAppoinmentId')
                ->select('lisAppoinments.*', 'lisTypeAppoinment.type')
                ->where('lisAppoinments.lisAppoinmentsId', '=', $id)->get();
            $agent = DB::table('lisAppoinments')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisAppoinments.agentId')
                ->select('lisAgent.*')
                ->where('lisAppoinments.lisAppoinmentsId', '=', $id)->get();
            $call = DB::table('lisAppoinments')
                ->join('lisCall', 'lisCall.lisCallId', '=', 'lisAppoinments.callId')
                ->join('lisCaller', 'lisCallerId', '=', 'lisCall.callerId')
                ->select('lisCall.*', 'lisCaller.*')
                ->where('lisAppoinments.lisAppoinmentsId', '=', $id)->get();
            $pdf = PDF::loadView('Reportes.appoinment', compact('appoinment', 'agent', 'call'));
            return $pdf->stream('appoinment.pdf');
        } catch (\PDOException $th) {
            return response()->json(['PDF' => $th], 400);
        }
    }

}
