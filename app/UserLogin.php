<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{
    protected $table = 'userLogin';
    protected $primaryKey = 'userLoginId';
    public $timestamps = false;
}
