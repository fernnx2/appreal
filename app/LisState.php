<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisState extends Model
{
    protected $table = 'lisStates';
    protected $primaryKey = 'lisStatesId';
    protected $keyType = 'string';
    public $timestamps = false;
}
