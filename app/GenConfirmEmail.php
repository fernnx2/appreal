<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfirmEmail extends Model
{
    protected $table = 'genConfirmEMail';
    protected $primaryKey = 'genConfirmEmailId';
    public $timestamps = false;
}
