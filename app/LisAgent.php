<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LisAgentType;

class LisAgent extends Model
{
    protected $table = 'lisAgent';
    protected $primaryKey = 'lisAgentId';
    public $timestamps = false;



}
