<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\UserRequestDemo;
use App\Http\Util\ValidatorController;

class UserRequestDemoController extends Controller
{
    private $rules = [
        'firstName' => 'required',
        'lastName' => 'required',
        'company' => 'required',
        'email' => 'required',
        'employees' => 'required',
    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $userRequestDemo = UserRequestDemo::all();
            return response()->json($userRequestDemo, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $userRequestDemo = new UserRequestDemo;
            $userRequestDemo->firstName = $data['firstName'];
            $userRequestDemo->lastName = $data['lastName'];
            $userRequestDemo->company = $data['company'];
            $userRequestDemo->email = $data['email'];
            $userRequestDemo->phone = $data['phone'];
            $userRequestDemo->employees = $data['employees'];
            $userRequestDemo->observaciones = $data['observaciones'];
            $userRequestDemo->updatedById = $data['updatedById'];
            $userRequestDemo->createdById = $data['createdById'];
            $userRequestDemo->save();
            $id = $userRequestDemo->userRequestDemoId;
            return response()->json(['userRequestDemoId' => $id], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Error!"], 400);

        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userRequestDemo = UserRequestDemo::findOrFail($id);
        try {
            return response()->json($userRequestDemo, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userRequestDemo = UserRequestDemo::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $userRequestDemo->firstName = $data['firstName'];
            $userRequestDemo->lastName = $data['lastName'];
            $userRequestDemo->company = $data['company'];
            $userRequestDemo->email = $data['email'];
            $userRequestDemo->phone = $data['phone'];
            $userRequestDemo->employees = $data['employees'];
            $userRequestDemo->observaciones = $data['observaciones'];
            $userRequestDemo->updatedById = $data['updatedById'];
            $userRequestDemo->createdById = $data['createdById'];
            $userRequestDemo->modifyDateTime = now();
            $userRequestDemo->save();
            return response()->json(["update", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Error!"], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userRequestDemo = UserRequestDemo::findOrFail($id);
        try {
            $userRequestDemo->delete();
            return response()->json(["delete", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
