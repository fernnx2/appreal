<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LisAgenthasProperty;
use App\Http\Util\ValidatorController;

class LisAgenthasPropertyController extends Controller
{
    private $rules = [
        'agentId' => 'required',
        'propertyId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agentProperty = new LisAgenthasProperty();
            $agentProperty->agentId = $data['agentId'];
            $agentProperty->propertyId = $data['propertyId'];
            $agentProperty->updatedById = $data['updatedById'];
            $agentProperty->createdById = $data['createdById'];
            $agentProperty->save();
            return response()->json($agentProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => 'Fail'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $agentProperty = LisAgenthasProperty::where('agentId', $id)->get();
            return response()->json($agentProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByAgentId' => 'Fail'], 400);
        }
    }

    public function findByProperty($id)
    {

        try {
            $agentProperty = LisAgenthasProperty::where('propertyId', $id)->get();
            $agents = DB::table('lisAgenthasProperty')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisAgenthasProperty.agentId')
                ->select('lisAgent.*')
                ->where('lisAgenthasProperty.propertyId', '=', $id)
                ->get();

            for ($i = 0; $i < \sizeof($agentProperty); $i++) {
                for ($o = 0; $o < \sizeof($agents); $o++) {
                    $id = $agents[$o]->lisAgentId;
                    if ($agentProperty[$i]['agentId'] === $id) {
                        $agentProperty[$i]['agentId'] = $agents;
                    }
                }
            }
            return response()->json($agentProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByAgentId' => 'Fail'], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agentProperty = LisAgenthasProperty::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agentProperty->agentId = $data['agentId'];
            $agentProperty->propertyId = $data['propertyId'];
            $agentProperty->updatedById = $data['updatedById'];
            $agentProperty->createdById = $data['createdById'];
            $agentProperty->save();
            return response()->json($agentProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => 'Fail'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $agentProperty = LisAgenthasProperty::findOrFail($id);
            $agentProperty->delete();
            return response()->json(['Delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }
}
