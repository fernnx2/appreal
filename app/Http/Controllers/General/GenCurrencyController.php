<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenCurrency;
use App\Http\Util\ValidatorController;

class GenCurrencyController extends Controller
{

    private $rules = [
        'code' => 'required',
        'name' => 'required',
        'symbol' => 'required',
        'currencyRounding' => 'required',
        'decimalSeparator' => 'required',
        'decimalThousand' => 'required',
        'groupingSeparator' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genCurrency = GenCurrency::all();
            return response()->json($genCurrency, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genCurrency = new GenCurrency();
            $genCurrency->code = $data['code'];
            $genCurrency->name = $data['name'];
            $genCurrency->symbol = $data['symbol'];
            $genCurrency->currencyRounding = $data['currencyRounding'];
            $genCurrency->decimalSeparator = $data['decimalSeparator'];
            $genCurrency->decimalThousand = $data['decimalThousand'];
            $genCurrency->groupingSeparator = $data['groupingSeparator'];
            $genCurrency->save();
            return response()->json(["created", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genCurrency = GenCurrency::findorFail($id);
        try {
            return response()->json($genCurrency, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Fail"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $genCurrency = GenCurrency::findOrFail($id);
        $validator = \Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['created' => false, 'errors' => $validator->errors()->all()], 400);
        }
        try {
            $data = \json_decode($request->getContent(), true);
            $genCurrency->code = $data['code'];
            $genCurrency->name = $data['name'];
            $genCurrency->symbol = $data['symbol'];
            $genCurrency->currencyRounding = $data['currencyRounding'];
            $genCurrency->decimalSeparator = $data['decimalSeparator'];
            $genCurrency->decimalThousand = $data['decimalThousand'];
            $genCurrency->groupingSeparator = $data['groupingSeparator'];
            $genCurrency->modifyDateTime = now();
            $genCurrency->save();
            return response()->json(["update", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genCurrency = GenCurrency::findorFail($id);
        try {
            $genCurrency->delete();
            return response()->json(["delete", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
