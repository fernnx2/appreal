<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\LisRomhasProperty;

class LisRomhasPropertyController extends Controller
{
    private $rules = [
        'propertyId' => 'required',
        'romsId' => 'required',
        'size' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $romProperty = new LisRomhasProperty();
            $romProperty->propertyId = $data['propertyId'];
            $romProperty->romsId = $data['romsId'];
            $romProperty->size = $data['size'];
            $romProperty->long = $data['long'];
            $romProperty->width = $data['width'];
            $romProperty->updatedById = $data['updatedById'];
            $romProperty->createdById = $data['createdById'];
            $romProperty->save();
            return response()->json($romProperty, 200);
        } catch (\PDOException $th) {
            return response()->json(['Create' => 'Fail'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $roms = LisRomhasProperty::where('propertyId', $id)->get();
            return response()->json($roms, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByProperty' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $romProperty = LisRomhasProperty::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $romProperty->propertyId = $data['propertyId'];
            $romProperty->romsId = $data['romsId'];
            $romProperty->size = $data['size'];
            $romProperty->long = $data['long'];
            $romProperty->width = $data['width'];
            $romProperty->updatedById = $data['updatedById'];
            $romProperty->createdById = $data['createdById'];
            $romProperty->save();
            return response()->json(['Update' => 'Exit'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Update' => 'Fail'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $romProperty = LisRomhasProperty::findOrFail($id);
            $romProperty->delete();
            return response()->json(['Delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }
}
