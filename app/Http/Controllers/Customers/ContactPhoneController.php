<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\ContactPhone;

class ContactPhoneController extends Controller
{
    private $rules = [
        'contactPhoneTypeId' => 'required',
        'contactId' => 'required',
        'number' => 'required',
        'active' => 'required',
        'tenantId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Listar todos los phones no tiene sentido
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = \json_decode($request->getContent(), true);
            for ($i = 0; $i < sizeof($data); $i++) {
                $phone = new ContactPhone();
                $phone->contactPhoneTypeId = $data[$i]['contactPhoneTypeId'];
                $phone->contactId = $data[$i]['contactId'];
                $phone->number = $data[$i]['number'];
                $phone->active = $data[$i]['active'];
                $phone->tenantId = $data[$i]['tenantId'];
                $phone->updatedById = $data[$i]['updatedById'];
                $phone->createdById = $data[$i]['createdById'];
                $phone->save();
            }
            return response()->json(['created' => 'Exito'], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $phonesAgent = ContactPhone::with('contactPhoneType')->where('contactId', $id)->get();
            return response()->json($phonesAgent, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindPhones' => $th], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phone = ContactPhone::findOrFail($id);
        try {
            $data = \json_decode($request->getContent(), true);

            $phone->contactPhoneTypeId = $data['contactPhoneTypeId'];
            $phone->contactId = $data['contactId'];
            $phone->number = $data['number'];
            $phone->active = $data['active'];
            $phone->tenantId = $data['tenantId'];
            $phone->updatedById = $data['updatedById'];
            $phone->createdById = $data['createdById'];
            $phone->modifyDateTime = now();
            $phone->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $phone = ContactPhone::findOrFail($id);
            $phone->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
