<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisStatus;
use App\Http\Util\ValidatorController;

class LisStatusController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'status' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $status = LisStatus::all();
            return response()->json($status, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $status = new LisStatus();
            $status->status = $data['status'];
            $status->tenantId = $data['tenantId'];
            $status->description = $data['description'];
            $status->updatedById = $data['updatedById'];
            $status->createdById = $data['createdById'];
            $status->save();
            return response()->json($status, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $status = LisStatus::findOrFail($id);
            return response()->json($status, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $status = LisStatus::where('tenantId', $id)->get();
            return response()->json($status, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = LisStatus::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $status->status = $data['status'];
            $status->tenantId = $data['tenantId'];
            $status->description = $data['description'];
            $status->updatedById = $data['updatedById'];
            $status->createdById = $data['createdById'];
            $status->modifyDateTime = now();
            $status->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $status = LisStatus::findOrFail($id);
            $status->delete();
            return response()->json(['delete' => 'Exit'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
