<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppForm extends Model
{
    protected $table = 'appForm';
    protected $primaryKey = 'appFormId';
    public $timestamps = false;
}
