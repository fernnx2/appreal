<?php

use Faker\Generator as Faker;

$factory->define(App\UserRequestDemo::class, function (Faker $faker) {
    return [
        'firstName' => $faker->name,
        'lastName' => $faker->name,
        'company' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->name,
        'employees' => $faker->randomDigit,
        'observaciones' => $faker->text,
        'updatedById' => $faker->randomDigit,
        'createdById' => $faker->randomDigit

    ];
});
