<?php

use Faker\Generator as Faker;

$factory->define(App\GenRegion::class, function (Faker $faker) {
    return [
        'genCountryId' => $faker->randomDigit,
        'name' => $faker->name,
        'active' => $faker->randomDigit

    ];
});
