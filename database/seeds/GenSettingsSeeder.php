<?php

use Illuminate\Database\Seeder;

class GenSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genSettings')->insert([
            'genCountryId' => '1',
            'genCurrencyId' => '1',
            'genLanguagesId' => '1',
            'genTimezoneId' => '1',
        ]);
    }
}
