<?php

use Faker\Generator as Faker;

$factory->define(App\TenantBillingType::class, function (Faker $faker) {
    return [
        'description' => $faker->name,
        'active' => null,
        'updatedById' => null,
        'createdById' => 1
    ];
});
