<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisRoms extends Model
{
    protected $table = 'lisRoms';
    protected $primaryKey = 'lisRomsId';
    public $timestamps = false;
}
