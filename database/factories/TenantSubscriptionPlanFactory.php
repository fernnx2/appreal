<?php

use Faker\Generator as Faker;

$factory->define(App\TenantSubscriptionPlan::class, function (Faker $faker) {
    return [
        'code' => null,
        'name' => $faker->name,
        'active' => 1,
        'durationDays' => $faker->randomDigit,
        'updatedById' => null,
        'createdById' => 1
    ];
});
