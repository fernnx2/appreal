<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisOwnerhasProperty extends Model
{
    protected $table = 'lisOwnerhasProperty';
    protected $primaryKey = 'lisOwnerhasPropertyId';
    public $timestamps = false;
}
