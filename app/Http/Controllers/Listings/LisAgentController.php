<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisAgent;
use App\Http\Util\ValidatorController;
use Illuminate\Support\Facades\DB;
use Dompdf\Exception;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class LisAgentController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'agentTypeId' => 'required',
        'firstName' => 'required',
        'lastName' => 'required',
        'mlsid' => 'required',
        'miscid' => 'required',
        'email' => 'required',
        'active' => 'required',
        'address' => 'required',
        'createdById' => 'required',
        'updatedById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $agent = LisAgent::all();
            return response()->json($agent, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agent = new LisAgent();
            $agent->tenantId = $data['tenantId'];
            $agent->agentTypeId = $data['agentTypeId'];
            $agent->firstName = $data['firstName'];
            $agent->lastName = $data['lastName'];
            $agent->mlsid = $data['mlsid'];
            $agent->miscid = $data['miscid'];
            $agent->fax = $data['fax'];
            $agent->email = $data['email'];
            $agent->text = $data['text'];
            $agent->webpage = $data['webpage'];
            $agent->active = $data['active'];
            $agent->address = $data['address'];
            $agent->notes = $data['notes'];
            $agent->updatedById = $data['updatedById'];
            $agent->createdById = $data['createdById'];
            $agent->save();
            return response()->json($agent, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $agent = LisAgent::findOrFail($id);
            return response()->json($agent, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }

    public function findByTenant($id)
    {
        try {
            $agent = LisAgent::where('tenantId', '=', $id)->get();
            return response()->json($agent, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = LisAgent::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $agent->tenantId = $data['tenantId'];
            $agent->agentTypeId = $data['agentTypeId'];
            $agent->firstName = $data['firstName'];
            $agent->lastName = $data['lastName'];
            $agent->mlsid = $data['mlsid'];
            $agent->miscid = $data['miscid'];
            $agent->fax = $data['fax'];
            $agent->email = $data['email'];
            $agent->text = $data['text'];
            $agent->webpage = $data['webpage'];
            $agent->active = $data['active'];
            $agent->address = $data['address'];
            $agent->notes = $data['notes'];
            $agent->updatedById = $data['updatedById'];
            $agent->createdById = $data['createdById'];
            $agent->save();
            return response()->json($agent, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $agent = LisAgent::findOrFail($id);
            $agent->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }

    public function pdf($id)
    {
        try {
            $agent = LisAgent::findOrFail($id);
            $propertys = DB::table('lisAgenthasProperty')
                ->join('lisProperty', 'lisProperty.lisPropertyId', '=', 'lisAgenthasProperty.propertyId')
                ->join('lisTypeProperty', 'lisTypeProperty.lisTypePropertyId', '=', 'lisProperty.typePropertyId')
                ->select('lisProperty.*', 'lisTypeProperty.typeProperty')
                ->where('lisAgenthasProperty.agentId', '=', $id)
                ->get();
            $pdf = PDF::loadView('Reportes.agent', compact('agent', 'propertys'));
            return $pdf->stream('agent.pdf');
        } catch (Exception $th) {
            return response()->json(['PDF' => 'Fail'], 400);
        }
    }


    public function uploadPhoto(Request $request, $id)
    {
        try {

            $url = 'uploads/images/agent/' . $id . '/' . $request->file->getClientOriginalName();
            $path = public_path() . '/uploads/images/agent/' . $id . '/';
            $tenant = LisAgent::findOrFail($id);
            $tenant->urlImage = $url;
            $tenant->save();
            $request->file->move($path, $request->file->getClientOriginalName());
            return response()->json(['Upload' => "Exito"], 200);
        } catch (\Exception $th) {
            return response()->json(['Upload' => "Fail"], 400);
        }
    }

    public function deletePhoto($id)
    {
        try {
            $image = LisAgent::findOrFail($id);
            File::delete($image->urlImage);
            return response()->json(['Delete' => $image->urlImage]);
        } catch (Exception $th) {
            return response()->json(['Delete' => 'Fail']);
        }
    }


}
