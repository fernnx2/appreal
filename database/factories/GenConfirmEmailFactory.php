<?php

use Faker\Generator as Faker;
use App\GenConfirmEmail;

$factory->define(GenConfirmEmail::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'confirmKey' => $faker->name,
        'isConfirmed' => 1,
        'dateConfirmed' => now(),
        'deathline' => now(),
        'active' => 1
    ];
});
