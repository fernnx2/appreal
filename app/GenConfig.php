<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfig extends Model
{
    protected $table = 'genConfig';
    protected $primaryKey = 'genConfigId';
    public $timestamps = false;
}
