<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Agents;


class AgentsControllerTest extends TestCase
{
    public $agent = [
        'firstName' => 'fernando',
        'lastName' => 'menjivar',
        'mlsid' => '1234',
        'miscid' => '1234',
        'fax' => '1233432sds',
        'email' => 'devfernando95@outlook.es',
        'text' => 'esto es un testo',
        'webpage' => 'estoesunurl.com',
        'active' => 1,
        'address' => 'esto es una direccion',
        'notes' => 'esto es una nota'
    ];
    /**
     * 
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        //se crean 4 registros
        factory(Agents::class)->create(['firstName' => 'fernando', 'lastName' => 'menjivar']);
        factory(Agents::class, 3)->create();
    }

    public function testIndex()
    {
        $this->get('api/agents')->assertStatus(200)->assertJsonCount(4);
    }

    public function testStore()
    {
        $this->json('POST', 'api/agents', $this->agent)->assertStatus(200)->assertJson(['agentId' => 5]);
        $this->assertDatabaseHas('agents', $this->agent);
    }

    public function testShow()
    {
        $this->get('api/agents/1')->assertStatus(200)->assertJson(['firstName' => 'fernando', 'lastName' => 'menjivar']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/agents/1', $this->agent)->assertStatus(200);
        $this->assertDatabaseHas('agents', $this->agent);
    }

    public function testDestroy()
    {
        $this->delete('api/agents/1')->assertStatus(200);
        $this->assertDatabaseMissing('agents', ['firstName' => 'fernando', 'lastName' => 'menjivar']);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM agents;');
        DB::statement('ALTER TABLE agents auto_increment=1;');
    }

}
