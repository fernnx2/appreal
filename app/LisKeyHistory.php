<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisKeyHistory extends Model
{
    protected $table = 'lisKeyHistory';
    protected $primaryKey = 'lisKeyHistoryId';
    public $timestamps = false;
}
