<?php

use Faker\Generator as Faker;
use App\Phones;

$factory->define(Phones::class, function (Faker $faker) {
    return [
        'number' => $faker->randomNumber($nbDigits = null, $strict = false),
        'phoneTypeId' => 1
    ];
});
