<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfigNameGroup extends Model
{
    protected $table = 'genConfigNameGroup';
    protected $primaryKey = 'genConfigNameGroupId';
    public $timestamps = false;
}
