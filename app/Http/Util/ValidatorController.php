<?php

namespace App\Http\Util;

use Illuminate\Http\Request;

class ValidatorController
{

    private $validator;

    public function __construct(Request $request, $rules)
    {
        $this->validator = \Validator::make($request->all(), $rules);
    }

    public function validar()
    {
        if ($this->validator->fails()) {
            return response()->json(['created' => false, 'errors' => $validator->errors()->all()], 401);
        }
    }




}