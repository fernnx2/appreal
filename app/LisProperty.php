<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\lisImagePropertys;

class LisProperty extends Model
{
    protected $table = 'lisProperty';
    protected $primaryKey = 'lisPropertyId';
    public $timestamps = false;


    public function images()
    {
        return $this->hasMany(lisImagePropertys::class, 'propertyId');
    }
}
