<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\ContactType;

class ContactTypeController extends Controller
{
    private $rules = [
        'contactType' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $contacType = ContactType::all();
            return response()->json($contacType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $contactType = new ContactType();
            $contactType->contactType = $data['contactType'];
            $contactType->active = $data['active'];
            $contactType->updatedById = $data['updatedById'];
            $contactType->createdById = $data['createdById'];
            $contactType->save();
            return response()->json($contactType, 200);

        } catch (\PDOException $th) {
            return response()->json(['create' => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $contacType = ContactType::findOrFail($id);
            return response()->json($contacType, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contactType = ContactType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $contactType->contactType = $data['contactType'];
            $contactType->active = $data['active'];
            $contactType->updatedById = $data['updatedById'];
            $contactType->createdById = $data['createdById'];
            $contactType->modifyDateTime = now();
            $contactType->save();
            return response()->json($contactType, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => $th], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $contacType = ContactType::findOrFail($id);
            $contacType->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => $th], 400);
        }
    }
}
