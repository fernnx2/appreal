<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisOwnerContactXref extends Model
{
    protected $table = 'lisOwnerContactXref';
    protected $primaryKey = 'lisOwnerContactXrefId';
    public $timestamps = false;
}
