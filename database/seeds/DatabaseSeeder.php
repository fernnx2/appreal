<?php

use Illuminate\Database\Seeder;
use App\GenLanguage;
use App\GenTimezone;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GenCountrySeeder::class);
        $this->call(GenRegionSeeder::class);
        $this->call(GenCurrencySeeder::class);
        $this->call(GenLanguageSeeder::class);
        $this->call(GenTimezoneSeeder::class);
        $this->call(GenSettingsSeeder::class);

    }
}
