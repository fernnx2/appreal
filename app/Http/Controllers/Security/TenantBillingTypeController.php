<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TenantBillingType;
use App\Http\Util\ValidatorController;

class TenantBillingTypeController extends Controller
{
    private $rules = [
        'description' => 'required',
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tenantBillingType = TenantBillingType::all();
            return response()->json($tenantBillingType, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantBillingType = new TenantBillingType();
            $tenantBillingType->description = $data['description'];
            $tenantBillingType->active = $data['active'];
            $tenantBillingType->updatedById = $data['updatedById'];
            $tenantBillingType->createdById = $data['createdById'];
            $tenantBillingType->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tenantBillingType = TenantBillingType::findOrFail($id);
            return response()->json($tenantBillingType, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenantBillingType = TenantBillingType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenantBillingType->description = $data['description'];
            $tenantBillingType->active = $data['active'];
            $tenantBillingType->updatedById = $data['updatedById'];
            $tenantBillingType->createdById = $data['createdById'];
            $tenantBillingType->modifyDateTime = now();
            $tenantBillingType->save();
            return response()->json('updated', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["updated" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tenantBillingType = TenantBillingType::findOrFail($id);
            $tenantBillingType->delete();
            return response()->json(['delete' => 'Exito!'], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
