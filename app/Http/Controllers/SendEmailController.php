<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\UserRequestDemoSendEmail;
use Illuminate\Support\Facades\Mail;
use App\UserRequestDemo;
use TheSeer\Tokenizer\Exception;

class SendEmailController extends Controller
{
   
    //Email de confirmacion
    public function show($id)
    {
        try {
            $user = UserRequestDemo::where('email', '=', $id)->firstOrFail();
            $emailAddress = $user['email'];
            Mail::to($emailAddress)->send(new UserRequestDemoSendEmail($user));
            return response()->json(['email' => $emailAddress], 200);
        } catch (Exception $th) {
            return response()->json(['status' => 'Error'], 400);
        }


    }


}
