<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserLoginType;
use App\Http\Util\ValidatorController;

class UserLoginTypeController extends Controller
{
    private $rules = [
        'name' => 'required',
        'createdById' => 'required'

    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $types = UserLoginType::all();
            return response()->json($types, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $type = new UserLoginType();
            $type->name = $data['name'];
            $type->active = $data['active'];
            $type->updatedById = $data['updatedById'];
            $type->createdById = $data['createdById'];
            $type->save();
            return response()->json(['create' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Error'], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $types = UserLoginType::findOrFail($id);
            return response()->json($types, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = UserLoginType::findOrFail($id);
        $validator = \Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['created' => false, 'errors' => $validator->errors()->all()], 401);
        }
        try {
            $data = \json_decode($request->getContent(), true);
            $type->name = $data['name'];
            $type->active = $data['active'];
            $type->updatedById = $data['updatedById'];
            $type->createdById = $data['createdById'];
            $type->modifyDateTime = now();
            $type->save();
            return response()->json(['create' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Error'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $type = UserLoginType::findOrFail($id);
            $type->delete();
            return response()->json(['Delete' => 'Exito!'], 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }
}
