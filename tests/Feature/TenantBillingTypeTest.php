<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\TenantBillingType;


class TenantBillingTypeTest extends TestCase
{
    private $tenantBillingType = [
        'description' => "billing",
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1
    ];
    /**
     * .
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        factory(TenantBillingType::class)->create(['description' => 'test']);
        factory(TenantBillingType::class, 5)->create();
    }

    public function testIndex()
    {
        $this->get('api/tenantbillingtype')->assertStatus(200)->assertJsonCount(6);
    }

    public function testStore()
    {
        $this->json('POST', 'api/tenantbillingtype', $this->tenantBillingType)->assertStatus(200);
        $this->assertDatabaseHas('tenantBillingType', $this->tenantBillingType);
    }

    public function testShow()
    {
        $this->get('api/tenantbillingtype/1')->assertStatus(200)->assertJson(['description' => 'test']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/tenantbillingtype/1', $this->tenantBillingType)->assertStatus(200);
        $this->assertDatabaseHas('tenantBillingType', $this->tenantBillingType);
    }

    public function testDestroy()
    {
        $this->delete('api/tenantbillingtype/1')->assertStatus(200);
        $this->assertDatabaseMissing('tenantBillingType', $this->tenantBillingType);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM tenantBillingType;');
        DB::statement('ALTER TABLE tenantBillingType auto_increment=1;');
    }

}
