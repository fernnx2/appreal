<?php

use Faker\Generator as Faker;

$factory->define(App\GenCurrency::class, function (Faker $faker) {
    return [
        'code' => $faker->currencyCode,
        'name' => $faker->name,
        'symbol' => $faker->currencyCode,
        'currencyRounding' => $faker->randomDigit,
        'decimalSeparator' => ',',
        'decimalThousand' => ',',
        'groupingSeparator' => ','
    ];
});
