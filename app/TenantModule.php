<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantModule extends Model
{
    protected $table = 'tenantModule';
    protected $primaryKey = 'tenantModuleId';
    public $timestamps = false;
}
