<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Http\Util\ValidatorController;

class ContactController extends Controller

{
    private $rules = [
        'active' => 'required',
        'tenantId' => 'required',
        'createdById' => 'required',
    ];

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $contact = Contact::all();
            return response()->json($contact, 200);
        } catch (\PDOException $th) {
            return response()->json(['finAll' => 'Error!'], 400);
        }
    }


    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $contact = new Contact();
            $contact->contactDeptId = $data['contactDeptId'];
            $contact->contactTitleId = $data['contactTitleId'];
            $contact->contactGenderId = $data['contactGenderId'];
            $contact->accountId = $data['accountId'];
            $contact->preferredGenLanguageId = $data['preferredGenLanguageId'];
            $contact->preferredShipAddressId = $data['preferredShipAddressId'];
            $contact->preferredBillAddressId = $data['preferredBillAddressId'];
            $contact->preferredPhoneId = $data['preferredPhoneId'];
            $contact->name = $data['name'];
            $contact->lastName = $data['lastName'];
            $contact->email = $data['email'];
            $contact->emailAlt = $data['emailAlt'];
            $contact->webSite = $data['webSite'];
            $contact->dob = $data['dob'];
            $contact->notes = $data['notes'];
            $contact->active = $data['active'];
            $contact->tenantId = $data['tenantId'];
            $contact->updatedById = $data['updatedById'];
            $contact->createdById = $data['createdById'];
            $contact->refNumberId = $data['refNumberId'];
            $contact->contactTypeId = $data['contactTypeId'];
            $contact->datebirthday = $data['datebirthday'];
            $contact->save();
            $contactId = $contact->contactId;
            return response()->json(["contactId", $contactId], 200);
        } catch (\PDOException $th) {
            return response()->json($th, 400);
        }
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $contact = Contact::with('contactPhone.contactPhoneType')->where('contactId', $id)->get();
            return response()->json($contact, 200);
        } catch (\PDOException $th) {
            return response()->json(['finAll' => 'Error!'], 400);
        }
    }



    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $contact->contactDeptId = $data['contactDeptId'];
            $contact->contactTitleId = $data['contactTitleId'];
            $contact->contactGenderId = $data['contactGenderId'];
            $contact->accountId = $data['accountId'];
            $contact->preferredGenLanguageId = $data['preferredGenLanguageId'];
            $contact->preferredShipAddressId = $data['preferredShipAddressId'];
            $contact->preferredBillAddressId = $data['preferredBillAddressId'];
            $contact->preferredPhoneId = $data['preferredPhoneId'];
            $contact->name = $data['name'];
            $contact->lastName = $data['lastName'];
            $contact->email = $data['email'];
            $contact->emailAlt = $data['emailAlt'];
            $contact->webSite = $data['webSite'];
            $contact->dob = $data['dob'];
            $contact->notes = $data['notes'];
            $contact->active = $data['active'];
            $contact->tenantId = $data['tenantId'];
            $contact->updatedById = $data['updatedById'];
            $contact->createdById = $data['createdById'];
            $contact->modifyDateTime = now();
            $contact->refNumberId = $data['refNumberId'];
            $contact->contactTypeId = $data['contactTypeId'];
            $contact->datebirthday = $data['datebirthday'];
            $contact->save();
            return response()->json(['updated' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['updated' => 'Fail'], 400);
        }
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $contact = Contact::findOrFail($id);
            $contact->delete();
            return response()->json('Delete', 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }
}
