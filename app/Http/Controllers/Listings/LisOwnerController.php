<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\LisOwner;

class LisOwnerController extends Controller
{
    private $rules = [
        'firstName' => 'required',
        'tenantId' => 'required',
        'lastName' => 'required',
        'notify' => 'required',
        'email' => 'required',
        'address' => 'required',
        'city' => 'required',
        'statesId' => 'required',
        'zipcode' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $owner = LisOwner::all();
            return response()->json($owner, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $owner = new LisOwner();
            $owner->statesId = $data['statesId'];
            $owner->tenantId = $data['tenantId'];
            $owner->firstName = $data['firstName'];
            $owner->lastName = $data['lastName'];
            $owner->notify = $data['notify'];
            $owner->email = $data['email'];
            $owner->text = $data['text'];
            $owner->sendReschShow = $data['sendReschShow'];
            $owner->showingRequest = $data['showingRequest'];
            $owner->address = $data['address'];
            $owner->city = $data['city'];
            $owner->zipcode = $data['zipcode'];
            $owner->notes = $data['notes'];
            $owner->updatedById = $data['updatedById'];
            $owner->createdById = $data['createdById'];
            $owner->save();
            return response()->json($owner, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $owner = LisOwner::findOrFail($id);
            return response()->json($owner, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }

    public function findByTenant($id)
    {
        try {
            $owner = LisOwner::where('tenantId', $id)->get();
            return response()->json($owner, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Fail'], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $owner = LisOwner::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $owner->statesId = $data['statesId'];
            $owner->tenantId = $data['tenantId'];
            $owner->firstName = $data['firstName'];
            $owner->lastName = $data['lastName'];
            $owner->notify = $data['notify'];
            $owner->email = $data['email'];
            $owner->text = $data['text'];
            $owner->sendReschShow = $data['sendReschShow'];
            $owner->showingRequest = $data['showingRequest'];
            $owner->address = $data['address'];
            $owner->city = $data['city'];
            $owner->zipcode = $data['zipcode'];
            $owner->notes = $data['notes'];
            $owner->updatedById = $data['updatedById'];
            $owner->createdById = $data['createdById'];
            $owner->modifyDateTime = now();
            $owner->save();
            return response()->json($owner, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $owner = LisOwner::findOrFail($id);
            $owner->delete();
            return response()->json(['Delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Fail'], 400);
        }
    }
}
