<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContactPhoneType;

class ContactPhone extends Model
{
    protected $table = 'contactPhone';
    protected $primaryKey = 'contactPhoneId';
    public $timestamps = false;


    public function contactPhoneType()
    {
        return $this->belongsTo(ContactPhoneType::class, 'contactPhoneTypeId');
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contactId');
    }
}
