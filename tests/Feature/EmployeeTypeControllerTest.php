<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\EmployeeType;


class EmployeeTypeControllerTest extends TestCase
{
    /**
     * 
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        //Poblando la bd para cada test
        factory(EmployeeType::class)->create([
            'name' => 'Sales'
        ]);
        factory(EmployeeType::class)->create([
            'name' => 'Manager'
        ]);
        factory(EmployeeType::class)->create([
            'name' => 'Ecomerce'
        ]);
    }

    public function testIndex()
    {
        $this->json('GET', 'api/employeetype')->assertJsonCount(3)->assertStatus(200);
    }

    public function tearDown()
    {
        DB::statement('DELETE FROM employeeType;');
        DB::statement('ALTER TABLE employeeType auto_increment=1;');
    }

}
