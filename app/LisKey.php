<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisKey extends Model
{
    protected $table = 'lisKey';
    protected $primaryKey = 'lisKeyId';
    public $timestamps = false;

}
