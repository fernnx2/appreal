<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserLoginRole;
use App\Http\Util\ValidatorController;

class UserLoginRoleController extends Controller
{
    private $rules = [
        'name' => 'required',
        'active' => 'required',
        'createdById' => 'required'

    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roles = UserLoginRole::all();
            return response()->json($roles, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }

    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $role = new UserLoginRole();
            $role->name = $data['name'];
            $role->code = $data['code'];
            $role->active = $data['active'];
            $role->updatedById = $data['updatedById'];
            $role->createdById = $data['createdById'];
            $role->save();
            return response()->json(['create' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Error'], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $roles = UserLoginRole::findOrFail($id);
            return response()->json($roles, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = UserLoginRole::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $role->name = $data['name'];
            $role->code = $data['code'];
            $role->active = $data['active'];
            $role->updatedById = $data['updatedById'];
            $role->createdById = $data['createdById'];
            $role->modifyDateTime = now();
            $role->save();
            return response()->json(['create' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Error'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $role = UserLoginRole::findOrFail($id);
            $role->delete();
            return response()->json(['Delete' => 'Exito!'], 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }
}
