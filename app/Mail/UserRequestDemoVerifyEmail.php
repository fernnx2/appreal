<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\UserRequestDemo;

class UserRequestDemoVerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $userRequestDemoVerify;
    public function __construct(UserRequestDemo $userRequestDemoVerify)
    {
        $this->userRequestDemoVerify = $userRequestDemoVerify;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $address = 'devfernando95@gmail.com';
        $subject = 'Mensaje de Verificacion!';
        $name = 'RealDeal';

        return $this->view('mails.userRequestDemoVerify_call')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with(['message' => $this->userRequestDemoVerify]);
    }
}
