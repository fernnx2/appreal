<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenRegion;
use App\Http\Util\ValidatorController;

class GenRegionController extends Controller
{
    private $rules = [
        'genCountryId' => 'required',
        'name' => 'required',
        'active' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genRegion = GenRegion::all();
            return response()->json($genRegion, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genRegion = new GenRegion();
            $genRegion->genCountryId = $data['genCountryId'];
            $genRegion->name = $data['name'];
            $genRegion->active = $data['active'];
            $genRegion->save();
            return response()->json(["create", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genRegion = GenRegion::findOrFail($id);
        try {
            return response()->json($genRegion, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Fail"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $genRegion = GenRegion::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genRegion->genCountryId = $data['genCountryId'];
            $genRegion->name = $data['name'];
            $genRegion->active = $data['active'];
            $genRegion->modifyDateTime = now();
            $genRegion->save();
            return response()->json(["update", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genRegion = GenRegion::findOrFail($id);
        try {
            $genRegion->delete();
            return response()->json(["delete", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
