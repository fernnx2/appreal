<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisAppoinments extends Model
{
    protected $table = 'lisAppoinments';
    protected $primaryKey = 'isAppoinmentsId';
    public $timestamps = false;
}
