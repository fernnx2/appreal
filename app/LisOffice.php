<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisOffice extends Model
{
    protected $table = 'lisOffice';
    protected $primaryKey = 'lisOfficeId';
    public $timestamps = false;
}
