<?php

use Faker\Generator as Faker;

$factory->define(App\UserLoginRoleXref::class, function (Faker $faker) {
    return [
        'userLoginRoleId' => $faker->numberBetween(1, 3),
        'userLoginId' => $faker->numberBetween(1, 3),
        'active' => 1,
        'tenantId' => $faker->numberBetween(1, 3),
        'updatedById' => 1,
        'createdById' => 1,
    ];
});
