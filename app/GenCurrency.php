<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenCurrency extends Model
{
    protected $table = 'genCurrency';
    protected $primaryKey = 'genCurrencyId';
    public $timestamps = false;
}
