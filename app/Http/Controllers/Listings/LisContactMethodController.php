<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisContactMethod;
use App\Http\Util\ValidatorController;

class LisContactMethodController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'name' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $method = LisContactMethod::all();
            return response()->json($method, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $method = new LisContactMethod();
            $method->name = $data['name'];
            $method->tenantId = $data['tenantId'];
            $method->description = $data['description'];
            $method->updatedById = $data['updatedById'];
            $method->createdById = $data['createdById'];
            $method->save();
            return response()->json($method, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $method = LisContactMethod::findOrFail($id);
            return response()->json($method, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $method = LisContactMethod::where('tenantId', $id)->get();
            return response()->json($method, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByTenantId' => 'Fail'], 400);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $method = LisContactMethod::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = json_decode($request->getContent(), true);
            $method->name = $data['name'];
            $method->tenantId = $data['tenantId'];
            $method->description = $data['description'];
            $method->updatedById = $data['updatedById'];
            $method->createdById = $data['createdById'];
            $method->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $method = LisContactMethod::findOrFail($id);
            $method->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
