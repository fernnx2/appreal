<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\LisOfficeType;

class LisOfficeTypeController extends Controller
{
    private $rules = [

        'type' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'

    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $type = LisOfficeType::all();
            return response()->json($type, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $type = new LisOfficeType();
            $type->type = $data['type'];
            $type->description = $data['description'];
            $type->updatedById = $data['updatedById'];
            $type->createdById = $data['createdById'];
            $type->save();
            return response()->json($type, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $type = LisOfficeType::findOrFail($id);
            return response()->json($type, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = LisOfficeType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $type->type = $data['type'];
            $type->description = $data['description'];
            $type->updatedById = $data['updatedById'];
            $type->createdById = $data['createdById'];
            $type->save();
            return response()->json($type, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $type = LisOfficeType::findOrFail($id);
            $type->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
