<?php

namespace App\Http\Controllers\Security;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use App\Tenant;
use App\Http\Util\ValidatorController;


class TenantController extends Controller
{
    private $rules = [
        'tenantSubscriptionPlanId' => 'required',
        'tenantStatusId' => 'required',
        'tenantSourceId' => 'required',
        'companyName' => 'required',
        'companyURL' => 'required',
        'database' => 'required',
        'allocatedUsers' => 'required',
        'active' => 'required',
        'createdById' => 'required',
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tenant = Tenant::all();
            return response()->json($tenant, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $tenant = new Tenant();
            $tenant->tenantSubscriptionPlanId = $data['tenantSubscriptionPlanId'];
            $tenant->tenantStatusId = $data['tenantStatusId'];
            $tenant->tenantSourceId = $data['tenantSourceId'];
            $tenant->companyName = $data['companyName'];
            $tenant->companyURL = $data['companyURL'];
            $tenant->database = $data['database'];
            $tenant->allocatedUsers = $data['allocatedUsers'];
            $tenant->billingRefNumber = $data['billingRefNumber'];
            $tenant->active = $data['active'];
            $tenant->updatedById = $data['updatedById'];
            $tenant->createdById = $data['createdById'];
            $tenant->tenentBillingTypeId = $data['tenentBillingTypeId'];
            $tenant->save();
            $tenantId = $tenant->tenantId;
            return response()->json(["tenantId", $tenantId], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $tenant = DB::table('tenant')->where('companyName', '=', $id)->get();
            return response()->json($tenant, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Error!"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenant = Tenant::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $tenant->tenantSubscriptionPlanId = $data['tenantSubscriptionPlanId'];
            $tenant->tenantStatusId = $data['tenantStatusId'];
            $tenant->tenantSourceId = $data['tenantSourceId'];
            $tenant->companyName = $data['companyName'];
            $tenant->companyURL = $data['companyURL'];
            $tenant->database = $data['database'];
            $tenant->allocatedUsers = $data['allocatedUsers'];
            $tenant->billingRefNumber = $data['billingRefNumber'];
            $tenant->active = $data['active'];
            $tenant->updatedById = $data['updatedById'];
            $tenant->createdById = $data['createdById'];
            $tenant->modifyDateTime = now();
            $tenant->tenentBillingTypeId = $data['tenentBillingTypeId'];
            $tenant->save();
            return response()->json('created', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tenant = Tenant::findOrFail($id);
            $tenant->delete();
            return response()->json('delete', 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }


    public function uploadLogo(Request $request, $id)
    {
        try {

            $url = 'uploads/images/tenant/' . $id . '/' . $request->file->getClientOriginalName();
            $path = public_path() . '/uploads/images/tenant/' . $id . '/';
            $tenant = Tenant::findOrFail($id);
            $tenant->urlImage = $url;
            $tenant->save();
            $request->file->move($path, $request->file->getClientOriginalName());
            return response()->json(['Upload' => "Exito"], 200);
        } catch (\Exception $th) {
            return response()->json(['Upload' => "Fail"], 400);
        }
    }

}
