<?php

use Faker\Generator as Faker;

$factory->define(App\GenCountry::class, function (Faker $faker) {
    return [
        'country' => $faker->name
    ];
});
