<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $primaryKey = 'contactId';
    public $timestamps = false;

    public function contactPhone()
    {
        return $this->hasMany(ContactPhone::class, 'contactId');
    }
}
