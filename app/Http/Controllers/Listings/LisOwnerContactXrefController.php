<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisOwnerContactXref;
use App\Http\Util\ValidatorController;


class LisOwnerContactXrefController extends Controller
{
    private $rules = [
        'ownerId' => 'required',
        'contactId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $ownerContact = new LisOwnerContactXref();
            $ownerContact->ownerId = $data['ownerId'];
            $ownerContact->contactId = $data['contactId'];
            $ownerContact->updatedById = $data['updatedById'];
            $ownerContact->createdById = $data['createdById'];
            $ownerContact->save();
            return response()->json($ownerContact, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $ownercontact = LisOwnerContactXref::where('ownerId', $id)->get();
            return response()->json($ownercontact, 200);
        } catch (\PDOException $th) {
            return resoponse()->json(['find', 'Fail'], 400);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ownerContact = LisOwnerContactXref::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $ownerContact->ownerId = $data['ownerId'];
            $ownerContact->contactId = $data['contactId'];
            $ownerContact->updatedById = $data['updatedById'];
            $ownerContact->createdById = $data['createdById'];
            $ownerContact->save();
            return response()->json($ownerContact, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ownercontact = LisOwnerContactXref::findOrFail($id);
            $ownercontact->delete();
            return response()->json($ownercontact, 200);
        } catch (\PDOException $th) {
            return resoponse()->json(['find', 'Fail'], 400);
        }
    }
}
