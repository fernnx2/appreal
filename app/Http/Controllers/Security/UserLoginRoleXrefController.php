<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\UserLoginRoleXref;

class UserLoginRoleXrefController extends Controller
{
    private $rules = [
        'userLoginRoleId' => 'required',
        'userLoginId' => 'required',
        'active' => 'required',
        'tenantId' => 'required',
        'createdById' => 'required'

    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roles = UserLoginRoleXref::all();
            return response()->json($roles, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $role = new UserLoginRoleXref();
            $role->userLoginRoleId = $data['userLoginRoleId'];
            $role->userLoginId = $data['userLoginId'];
            $role->active = $data['active'];
            $role->tenantId = $data['tenantId'];
            $role->updatedById = $data['updatedById'];
            $role->createdById = $data['createdById'];
            $role->save();
            return response()->json(['create' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Error'], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $roles = UserLoginRoleXref::findOrFail($id);
            return response()->json($roles, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = UserLoginRoleXref::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $role->userLoginRoleId = $data['userLoginRoleId'];
            $role->userLoginId = $data['userLoginId'];
            $role->active = $data['active'];
            $role->tenantId = $data['tenantId'];
            $role->updatedById = $data['updatedById'];
            $role->createdById = $data['createdById'];
            $role->modifyDateTime = now();
            $role->save();
            return response()->json(['updated' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['updated' => 'Error'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $role = UserLoginRoleXref::findOrFail($id);
            $role->delete();
            return response()->json(['Delete' => 'Exito!'], 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }
}
