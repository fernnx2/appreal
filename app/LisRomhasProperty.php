<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisRomhasProperty extends Model
{
    protected $table = 'lisRomshasProperty';
    protected $primaryKey = 'lisRomshasPropertyId';
    public $timestamps = false;
}
