<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisNewListing extends Model
{
    protected $table = 'lisNewListing';
    protected $primaryKey = 'lisNewListingId';
    public $timestamps = false;
}
