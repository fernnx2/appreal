<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\GenRegion;

class GenRegionControllerTest extends TestCase
{
    /**
     * El metodo setUp se ejecutara en cada vez que se realize un test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        factory(GenRegion::class)->create([
            'genCountryId' => 1,
            'name' => 'central',
            'active' => 1,
        ]);
        factory(GenRegion::class)->create([
            'genCountryId' => 1,
            'name' => 'Occidental',
            'active' => 1,
        ]);
        factory(GenRegion::class)->create([
            'genCountryId' => 1,
            'name' => 'Oriental',
            'active' => 1,
        ]);

    }

    //TEST 

    public function testIndex()
    {
        $this->get('api/region')->assertStatus(200);
    }

    public function testStore()
    {
        $this->json('POST', 'api/region', [
            'genCountryId' => 1,
            'name' => 'ParaCentral',
            'active' => 1,
        ])->assertStatus(200);
        $this->assertDatabaseHas('genRegion', [
            'genCountryId' => 1,
            'name' => 'ParaCentral',
            'active' => 1,
        ]);
    }

    public function testShow()
    {
        $this->get('api/region/1')->assertStatus(200);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/region/1', [
            'genCountryId' => 1,
            'name' => 'ParaCentral',
            'active' => 1,
        ])->assertStatus(200);
        $this->assertDatabaseHas('genRegion', [
            'genCountryId' => 1,
            'name' => 'ParaCentral',
            'active' => 1,
        ]);
    }

    public function testDestroy()
    {
        $this->delete('api/region/3')->assertStatus(200);
        $this->assertDatabaseMissing('genRegion', [
            'genCountryId' => 1,
            'name' => 'Oriental',
            'active' => 1,
        ]);

    }
    public function tearDown()
    {
        DB::statement('DELETE FROM genRegion;');
        DB::statement('ALTER TABLE genRegion auto_increment=1;');
    }
}
