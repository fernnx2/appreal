<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentKey extends Model
{
    protected $table = 'agentKey';
    protected $primaryKey = 'agentKeyId';
    public $timestamps = false;
}
