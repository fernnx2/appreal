<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoginPermission extends Model
{
    protected $table = 'userLoginPermission';
    protected $primaryKey = 'userPermissionId';
    public $timestamps = false;
}
