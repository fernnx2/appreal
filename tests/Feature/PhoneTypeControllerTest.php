<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PhoneType;

class PhoneTypeControllerTest extends TestCase
{
    public $type = [
        'type' => 'celular',
        'description' => 'celular movil'
    ];
    /**
     * 
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        factory(PhoneType::class)->create(['type' => 'personal']);
        factory(PhoneType::class)->create(['type' => 'casa']);
        factory(PhoneType::class)->create(['type' => 'oficina']);
    }

    public function testIndex()
    {
        $this->get('api/phonetype')->assertStatus(200)->assertJsonCount(3);
    }
    public function testStore()
    {
        $this->json('POST', 'api/phonetype', $this->type)->assertStatus(200);
        $this->assertDatabaseHas('phoneType', $this->type);
    }
    public function testShow()
    {
        $this->get('api/phonetype/1')->assertStatus(200)->assertJson(['type' => 'personal']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/phonetype/1', $this->type)->assertStatus(200);
        $this->assertDatabaseHas('phoneType', $this->type);
    }

    public function testDestroy()
    {
        $this->delete('api/phonetype/1')->assertStatus(200);
        $this->assertDatabaseMissing('phoneType', ['type' => 'personal']);
    }


    public function tearDown()
    {
        DB::statement('DELETE FROM phoneType;');
        DB::statement('ALTER TABLE phoneType auto_increment=1;');
    }

}
