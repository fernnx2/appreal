<?php

use Faker\Generator as Faker;

$factory->define(App\TenantSource::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1
    ];
});
