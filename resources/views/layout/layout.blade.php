<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="css/app.css" rel="stylesheet">
    </head>
    <body>
    <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	@yield('content')
                </div>
            </div>
        </div>
    </body>
</html>