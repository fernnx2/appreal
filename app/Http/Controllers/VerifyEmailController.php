<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRequestDemoVerifyEmail;
use App\UserRequestDemo;

class VerifyEmailController extends Controller
{
 

    // Email de verificacion
    public function show($id)
    {
        try {
            $user = UserRequestDemo::where('email', '=', $id)->firstOrFail();
            $emailAddress = $user['email'];
            Mail::to($emailAddress)->send(new UserRequestDemoVerifyEmail($user));
            return response()->json($user, 200);
        } catch (Exception $th) {
            return response()->json(['status' => 'Error'], 400);
        }

    }


}
