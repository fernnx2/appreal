<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Tenant;
use App\TenantSource;
use App\TenantStatus;
use App\TenantSubscriptionPlan;

class TenantControllerTest extends TestCase
{
    private $tenant = [
        'tenantSubscriptionPlanId' => 1,
        'tenantStatusId' => 1,
        'tenantSourceId' => 1,
        'companyName' => 'dev',
        'companyURL' => 'http://devfernando.ga',
        'database' => 'realapp',
        'allocatedUsers' => 2,
        'billingRefNumber' => '1',
        'active' => 1,
        'updatedById' => null,
        'createdById' => 1,
        'tenentBillingTypeId' => null
    ];
    /**
     * 
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        factory(TenantSource::class, 3)->create();
        factory(TenantStatus::class, 3)->create();
        factory(TenantSubscriptionPlan::class)->create(['name' => 'premiun']);
        factory(TenantSubscriptionPlan::class)->create(['name' => 'gratis']);
        factory(Tenant::class)->create(['companyName' => 'devfernando']);
        factory(Tenant::class, 3)->create();

    }

    public function testIndex()
    {
        $this->get('api/tenant')->assertStatus(200)->assertJsonCount(4);
    }

    public function testStore()
    {
        $this->json('POST', 'api/tenant', $this->tenant)->assertStatus(200);
        $this->assertDatabaseHas('tenant', $this->tenant);
    }

    public function testShow()
    {
        $this->get('api/tenant/1')->assertStatus(200)->assertJson(['companyName' => 'devfernando']);
    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/tenant/1', $this->tenant)->assertStatus(200);
        $this->assertDatabaseHas('tenant', $this->tenant);
    }

    public function testDestroy()
    {
        $this->delete('api/tenant/1')->assertStatus(200);
        $this->assertDatabaseMissing('tenant', ['companyName' => 'devfernando']);
    }


    public function tearDown()
    {
        DB::statement('DELETE FROM tenant;');
        DB::statement('ALTER TABLE tenant auto_increment=1;');
    }
}
