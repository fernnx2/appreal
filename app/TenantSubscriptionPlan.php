<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantSubscriptionPlan extends Model
{
    protected $table = 'tenantSubscriptionPlan';
    protected $primaryKey = 'tenantSubscriptionPlanId';
    public $timestamps = false;
}
