<?php

namespace App\Http\Controllers\UserMail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\UserLogin;
use App\Mail\UserLoginRecoverPass;

class UserLoginRecoverPassController extends Controller
{


    /**
     * Envindo un email con el password
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userLogin = UserLogin::where('email', '=', $id)->firstOrFail();
            $emailAddress = $userLogin['email'];
            Mail::to($emailAddress)->send(new UserLoginRecoverPass($userLogin));
            return response()->json(['status send mail' => 'Email de Passowrd Recover enviado Exitosamente'], 200);
        } catch (\PDOException $th) {
            return response()->json(['status send email' => 'Fail'], 400);
        }
    }


}
