<?php

use Faker\Generator as Faker;
use App\PhoneType;

$factory->define(PhoneType::class, function (Faker $faker) {
    return [
        'type' => $faker->tollFreePhoneNumber,
        'description' => $faker->catchPhrase
    ];
});
