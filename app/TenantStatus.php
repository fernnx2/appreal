<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantStatus extends Model
{
    protected $table = 'tenantStatus';
    protected $primaryKey = 'tenantStatusId';
    public $timestamps = false;
}
