<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisTypeAppoinment extends Model
{
    protected $table = 'lisTypeAppoinment';
    protected $primaryKey = 'lisTypeAppoinmentId';
    public $timestamps = false;
}
