<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LisCall;
use App\Http\Util\ValidatorController;
use Barryvdh\DomPDF\Facade as PDF;

class LisCallController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'propertyId' => 'required',
        'contactMethodId' => 'required',
        'categorySourceContactId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $calls = LisCall::all();
            return response()->json($calls, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $call = new LisCall();
            $call->tenantId = $data['tenantId'];
            $call->reasonCall = $data['reasonCall'];
            $call->propertyId = $data['propertyId'];
            $call->agentId = $data['agentId'];
            $call->followUp = $data['followUp'];
            $call->offer = $data['offer'];
            $call->contactMethodId = $data['contactMethodId'];
            $call->categorySourceContactId = $data['categorySourceContactId'];
            $call->callerId = $data['callerId'];
            $call->updatedById = $data['updatedById'];
            $call->createdById = $data['createdById'];
            $call->save();
            return response()->json($call, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $calls = LisCall::findOrFail($id);
            return response()->json($calls, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    public function findByTenantId($id)
    {
        try {
            $calls = LisCall::where('tenantId', $id)->get();
            return response()->json($calls, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }

    public function findByTenantLeadFilter($id)
    {
        try {
            $calls = DB::table('lisCall')
                ->join('lisContactMethod', 'lisContactMethod.lisContactMethodId', '=', 'lisCall.contactMethodId')
                ->join('lisCategorySourceContact', 'lisCategorySourceContact.lisCategorySourceContactId', '=', 'lisCall.categorySourceContactId')
                ->join('lisSourceContact', 'lisSourceContact.lisSourceContactId', '=', 'lisCategorySourceContact.sourceContactId')
                ->join('lisCaller', 'lisCallerId', '=', 'lisCall.callerId')
                ->join('lisProperty', 'lisProperty.lisPropertyId', '=', 'lisCall.propertyId')
                ->join('lisAgenthasProperty', 'lisAgenthasProperty.propertyId', '=', 'lisProperty.lisPropertyId')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisAgenthasProperty.agentId')
                ->select(
                    'lisCall.*',
                    'lisCaller.firstName',
                    'lisCaller.lastName',
                    'lisCaller.phone',
                    'lisAgent.firstName',
                    'lisAgent.lastName',
                    'lisContactMethod.name',
                    'lisCategorySourceContact.category',
                    'lisSourceContact.source'
                )
                ->where('lisCall.tenantId', '=', $id)
                ->get();
            return response()->json($calls, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => $th], 400);
        }
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $call = LisCall::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $call->tenantId = $data['tenantId'];
            $call->reasonCall = $data['reasonCall'];
            $call->propertyId = $data['propertyId'];
            $call->agentId = $data['agentId'];
            $call->followUp = $data['followUp'];
            $call->offer = $data['offer'];
            $call->contactMethodId = $data['contactMethodId'];
            $call->categorySourceContactId = $data['categorySourceContactId'];
            $call->callerId = $data['callerId'];
            $call->updatedById = $data['updatedById'];
            $call->createdById = $data['createdById'];
            $call->modifyDateTime = now();
            $call->save();
            return response()->json(['update' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['update' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $calls = LisCall::findOrFail($id);
            $calls->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }

    public function pdf($id)
    {
        try {
            $call = LisCall::findOrFail($id);
            $property = DB::table('lisCall')
                ->join('lisProperty', 'lisProperty.lisPropertyId', '=', 'lisCall.propertyId')
                ->select('lisProperty.*')
                ->where('lisCall.lisCallId', '=', $id)->get();
            $agent = DB::table('lisCall')
                ->join('lisAgent', 'lisAgent.lisAgentId', '=', 'lisCall.agentId')
                ->select('lisAgent.*')
                ->where('lisCall.lisCallId', '=', $id)->get();
            $method = DB::table('lisCall')
                ->join('lisContactMethod', 'lisContactMethod.lisContactMethodId', '=', 'lisCall.contactMethodId')
                ->select('lisContactMethod.name')
                ->where('lisCall.lisCallId', '=', $id)->get();
            $source = DB::table('lisCall')
                ->join('lisCategorySourceContact', 'lisCategorySourceContact.lisCategorySourceContactId', '=', 'lisCall.CategorySourceContactId')
                ->join('lisSourceContact', 'lisSourceContact.lisSourceContactId', '=', 'lisCategorySourceContact.sourceContactId')
                ->select('lisCategorySourceContact.category', 'lisSourceContact.source')
                ->where('lisCall.lisCallId', '=', $id)->get();
            $pdf = PDF::loadView('Reportes.call', compact('call', 'property', 'agent', 'method', 'source'));
            return $pdf->stream('call.pdf');
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }
}
