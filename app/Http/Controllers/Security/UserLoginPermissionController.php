<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserLoginPermission;
use App\Http\Util\ValidatorController;

class UserLoginPermissionController extends Controller
{
    private $rules = [
        'createdById' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $ulp = UserLoginPermission::all();
            return response()->json($ulp, 200);

        } catch (\PDOException $th) {
            return response()->json(['findAll' => 'Error!'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $ulp = new UserLoginPermission();
            $ulp->tenantModuleId = $data['tenantModuleId'];
            $ulp->userLoginRoleId = $data['userLoginRoleId'];
            $ulp->appFormId = $data['appFormId'];
            $ulp->appOptionId = $data['appOptionId'];
            $ulp->userLoginId = $data['userLoginId'];
            $ulp->show = $data['show'];
            $ulp->multiShape = $data['multiShape'];
            $ulp->modify = $data['modify'];
            $ulp->tenandId = $data['tenandId'];
            $ulp->updatedById = $data['updatedById'];
            $ulp->createdById = $data['createdById'];
            $ulp->save();
            return response()->json(['create' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Error'], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $ulp = UserLoginPermission::findOrFail($id);
            return response()->json($ulp, 200);

        } catch (\PDOException $th) {
            return response()->json(['find' => 'Error!'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ulp = UserLoginPermission::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $ulp->tenantModuleId = $data['tenantModuleId'];
            $ulp->userLoginRoleId = $data['userLoginRoleId'];
            $ulp->appFormId = $data['appFormId'];
            $ulp->appOptionId = $data['appOptionId'];
            $ulp->userLoginId = $data['userLoginId'];
            $ulp->show = $data['show'];
            $ulp->multiShape = $data['multiShape'];
            $ulp->modify = $data['modify'];
            $ulp->tenandId = $data['tenandId'];
            $ulp->updatedById = $data['updatedById'];
            $ulp->createdById = $data['createdById'];
            $ulp->modifyDateTime = now();
            $ulp->save();
            return response()->json(['updated' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['updated' => 'Error'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ulp = UserLoginPermission::findOrFail($id);
            $ulp->delete();
            return response()->json(['Delete' => 'Exito!'], 200);

        } catch (\PDOException $th) {
            return response()->json(['Delete' => 'Error!'], 400);
        }
    }
}
