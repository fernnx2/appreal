<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisCaller;
use App\Http\Util\ValidatorController;

class LisCallerController extends Controller
{
    private $rules = [
        'tenantId' => 'required',
        'firstName' => 'required',
        'lastName' => 'required',
        'status' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $callers = LisCaller::all();
            return response()->json($callers, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindAll' => 'Fail'], 400);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $caller = new LisCaller();
            $caller->tenantId = $data['tenantId'];
            $caller->firstName = $data['firstName'];
            $caller->lastName = $data['lastName'];
            $caller->companyName = $data['companyName'];
            $caller->email = $data['email'];
            $caller->phone = $data['phone'];
            $caller->address = $data['address'];
            $caller->city = $data['city'];
            $caller->statesId = $data['statesId'];
            $caller->zipcode = $data['zipcode'];
            $caller->status = $data['status'];
            $caller->notes = $data['notes'];
            $caller->updatedById = $data['updatedById'];
            $caller->createdById = $data['createdById'];
            $caller->save();
            return response()->json($caller, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $caller = LisCaller::findOrFail($id);
            return response()->json($caller, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindById' => 'Fail'], 400);
        }
    }
    public function findByTenantId($id)
    {
        try {
            $caller = LisCaller::where('tenantId', $id)->get();
            return response()->json($caller, 200);
        } catch (\PDOException $th) {
            return response()->json(['FindByTenantId' => 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $caller = LisCaller::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $caller->tenantId = $data['tenantId'];
            $caller->firstName = $data['firstName'];
            $caller->lastName = $data['lastName'];
            $caller->companyName = $data['companyName'];
            $caller->email = $data['email'];
            $caller->phone = $data['phone'];
            $caller->address = $data['address'];
            $caller->city = $data['city'];
            $caller->statesId = $data['statesId'];
            $caller->zipcode = $data['zipcode'];
            $caller->status = $data['status'];
            $caller->notes = $data['notes'];
            $caller->updatedById = $data['updateById'];
            $caller->createdById = $data['createdById'];
            $caller->modifyDateTime = now();
            $caller->save();
            return response()->json($caller, 200);
        } catch (\PDOException $th) {
            return response()->json(['create' => 'Fail'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $caller = LisCaller::findOrFail($id);
            $caller->delete();
            return response()->json(['delete' => 'Exito'], 200);
        } catch (\PDOException $th) {
            return response()->json(['delete' => 'Fail'], 400);
        }
    }
}
