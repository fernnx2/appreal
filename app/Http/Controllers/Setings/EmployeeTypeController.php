<?php

namespace App\Http\Controllers\Setings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmployeeType;
use App\Http\Util\ValidatorController;

class EmployeeTypeController extends Controller
{
    private $rules = [
        'name' => 'required'
    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $employeeType = EmployeeType::all();
            return response()->json($employeeType, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Error!"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $employeeType = new EmployeeType();
            $employeeType->name = $data['name'];
            $employeeType->save();
            return response()->json(["create", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Error!"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employeeType = EmployeeType::findOrFail($id);
        try {
            return response()->json($employeeType, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findById" => "Error!"], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employeeType = EmployeeType::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $employeeType->name = $data['name'];
            $employeeType->save();
            return response()->json(["create", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Error!"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employeeType = EmployeeType::findOrFail($id);
        try {
            $employeeType->delete($id);
            return response()->json(["delete", "Exito!"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Error!"], 400);
        }
    }
}
