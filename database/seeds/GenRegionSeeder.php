<?php

use Illuminate\Database\Seeder;

class GenRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genRegion')->insert([
            'genCountryId' => '1',
            'name' => 'Occidental',

        ]);
        DB::table('genRegion')->insert([
            'genCountryId' => '1',
            'name' => 'Oriental',

        ]);
        DB::table('genRegion')->insert([
            'genCountryId' => '1',
            'name' => 'Central',

        ]);
        DB::table('genRegion')->insert([
            'genCountryId' => '1',
            'name' => 'Paracentral',

        ]);
    }
}
