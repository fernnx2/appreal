<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantBillingType extends Model
{
    protected $table = 'tenantBillingType';
    protected $primaryKey = 'tenantBillingTypeId';
    public $timestamps = false;
}
