<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenSettings;
use App\Http\Util\ValidatorController;

class GenSettingsController extends Controller
{
    private $rules = [
        'genCountryId' => 'required',
        'genCurrencyId' => 'required',
        'genLanguagesId' => 'required',
        'genTimezoneId' => 'required'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genSettings = GenSettings::all();
            return response()->json($genSettings, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genSettings = new GenSettings();
            $genSettings->genCountryId = $data['genCountryId'];
            $genSettings->genCurrencyId = $data['genCurrencyId'];
            $genSettings->genLanguagesId = $data['genLanguagesId'];
            $genSettings->genTimezoneId = $data['genTimezoneId'];
            $genSettings->save();
            return response()->json(["create", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["create" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genSettings = GenSettings::findOrFail($id);
        try {
            return response()->json($genSettings, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["find" => "Fail"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $genSettings = GenSettings::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $genSettings->genCountryId = $data['genCountryId'];
            $genSettings->genCurrencyId = $data['genCurrencyId'];
            $genSettings->genLanguagesId = $data['genLanguagesId'];
            $genSettings->genTimezoneId = $data['genTimezoneId'];
            $genSettings->modifyDateTime = now();
            $genSettings->save();
            return response()->json(["update", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genSettings = GenSettings::findOrFail($id);
        try {
            $genSettings->delete();
            return response()->json(["delete", "Exito"], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
