<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LisAgentContactXref;
use App\Http\Util\ValidatorController;

class LisAgentContactXrefController extends Controller
{
    private $rules = [
        'agentId' => 'required',
        'contactId' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agentContact = new LisAgentContactXref();
            $agentContact->agentId = $data['agentId'];
            $agentContact->contactId = $data['contactId'];
            $agentContact->updatedById = $data['updatedById'];
            $agentContact->createdById = $data['createdById'];
            $agentContact->save();
            return response()->json($agentContact, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Devolvera un recurso en base al id del agente.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $agentcontact = LisAgentContactXref::where('agentId', $id)->get();
            return response()->json($agentcontact, 200);
        } catch (\PDOException $th) {
            return resoponse()->json(['find', 'Fail'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agentContact = LisAgentContactXref::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $agentContact->agentId = $data['agentId'];
            $agentContact->contactId = $data['contactId'];
            $agentContact->updatedById = $data['updatedById'];
            $agentContact->createdById = $data['createdById'];
            $agentContact->modifyDateTime = now();
            $agentContact->save();
            return response()->json($agentContact, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $agentcontact = LisAgentContactXref::findOrFail($id);
            $agentcontact->delete();
            return response()->json($agentcontact, 200);
        } catch (\PDOException $th) {
            return resoponse()->json(['find', 'Fail'], 400);
        }
    }
}
