<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\GenConfirmEmail;

class GenConfirmEmailControllerTest extends TestCase
{
    public $confirmEmail = [
        'email' => 'email',
        'confirmKey' => 'confirmKey',
        'isConfirmed' => 1,
        'deathline' => "2018-12-13 03:10:48",
        'active' => 1,
    ];

    /**
     * 
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        factory(GenConfirmEmail::class)->create(['email' => 'fernanndo@gmail.com']);
        factory(GenConfirmEmail::class, 2)->create();
    }

    public function testIndex()
    {
        $response = $this->get('api/confirmemail')->assertStatus(200)->assertJsonCount(3);
    }

    public function testStore()
    {
        $this->json('POST', 'api/confirmemail', $this->confirmEmail)->assertStatus(200);
        $this->assertDatabaseHas('genConfirmEMail', $this->confirmEmail);
    }


    public function testShow()
    {
        $response = $this->get('api/confirmemail/1')->assertStatus(200)->assertJson([
            'email' => 'fernanndo@gmail.com'
        ]);

    }

    public function testUpdate()
    {
        $this->json('PUT', 'api/confirmemail/1', $this->confirmEmail)->assertStatus(200);
        $this->assertDatabaseHas('genConfirmEMail', $this->confirmEmail);
    }

    public function testDestroy()
    {
        $this->delete('api/confirmemail/1')->assertStatus(200);
        $this->assertDatabaseMissing('genConfirmEMail', ['email' => 'fernanndo@gmail.com']);
    }


    public function tearDown()
    {
        DB::statement('DELETE FROM genConfirmEMail;');
        DB::statement('ALTER TABLE genConfirmEMail auto_increment=1;');
    }




}
