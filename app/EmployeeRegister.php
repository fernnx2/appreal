<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeRegister extends Model
{
    protected $table = 'employeeRegister';
    protected $primaryKey = 'employee2_Id';
    public $timestamps = false;
}
