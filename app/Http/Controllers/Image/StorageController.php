<?php

namespace App\Http\Controllers\Image;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\lisImagePropertys;


class StorageController extends Controller
{
    public function save(Request $request)
    {
        try {
            $imageProperty = new lisImagePropertys();
            $url = 'uploads/images/propertys/' . $request->propertyId . '/' . $request->file->getClientOriginalName();
            $path = public_path() . '/uploads/images/propertys/' . $request->propertyId . '/';
            $imageProperty->tenantId = $request->tenantId;
            $imageProperty->name = $request->file->getClientOriginalName();
            $imageProperty->url = $url;
            $imageProperty->propertyId = $request->propertyId;
            $imageProperty->updatedById = $request->updatedById;
            $imageProperty->createdById = $request->createdById;
            $imageProperty->save();
            $request->file->move($path, $request->file->getClientOriginalName());
            return response()->json(['Upload' => "Exito"], 200);
        } catch (\Exception $th) {
            return response()->json(['Save Image' => $th], 400);
        }


    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * 
     */
    public function findByProperty($id)
    {
        try {
            $images = lisImagePropertys::where('propertyId', '=', $id)->get();
            return response()->json($images, 200);
        } catch (Exception $th) {
            return response()->json(['FindImage' => th], 400);
        }
    }

    public function delete($id)
    {
        try {
            $image = lisImagePropertys::findOrFail($id);
            File::delete($image->url);
            $image->delete();
            return response()->json(['Delete' => $image->url]);
        } catch (Exception $th) {
            return response()->json(['Delete' => 'Fail']);
        }
    }
}
