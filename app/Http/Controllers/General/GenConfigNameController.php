<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfigName;
use App\Http\Util\ValidatorController;

class GenConfigNameController extends Controller
{
    private $rules = [
        'genConfigNameGroupId' => 'required',
        'genConfigNameValueTypeId' => 'required',
        'name' => 'required',
        'active' => 'required',

    ];
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $configName = GenConfigName::all();
            return response()->json($configName, 200);
        } catch (\PDOException $th) {
            return response()->json(["findAll" => "Fail"], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $configName = new GenConfigName();
            $configName->genConfigNameGroupId = $data['genConfigNameGroupId'];
            $configName->genConfigNameSubGroupId = $data['genConfigNameSubGroupId'];
            $configName->genConfigNameValueTypeId = $data['genConfigNameValueTypeId'];
            $configName->name = $data['name'];
            $configName->description = $data['description'];
            $configName->code = $data['code'];
            $configName->defaultValue = $data['defaultValue'];
            $configName->active = $data['active'];
            $configName->updatedById = $data['updatedById'];
            $configName->createdById = $data['createdById'];
            $configName->controlText = $data['controlText'];
            $configName->controlminlength = $data['controlminlength'];
            $configName->controlmaxlength = $data['controlmaxlength'];
            $configName->imageMinWidth = $data['imageMinWidth'];
            $configName->imageMinHeight = $data['imageMinHeight'];
            $configName->controlmask = $data['controlmask'];
            $configName->controlTable = $data['controlTable'];
            $configName->controlkey = $data['controlkey'];
            $configName->controlFilterByTenant = $data['controlFilterByTenant'];
            $configName->controlvalue = $data['controlvalue'];
            $configName->isVisible = $data['isVisible'];
            $configName->save();
            $genConfigNameId = $configName->genConfigNameId;
            return response()->json(["genConfigNameId", $genConfigNameId], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $configName = GenConfigName::findOrFail($id);
            return response()->json($configName, 200);
        } catch (\PDOException $th) {
            return response()->json(["find" => "Error!"], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $configName = GenConfigName::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);

            $configName->genConfigNameGroupId = $data['genConfigNameGroupId'];
            $configName->genConfigNameSubGroupId = $data['genConfigNameSubGroupId'];
            $configName->genConfigNameValueTypeId = $data['genConfigNameValueTypeId'];
            $configName->name = $data['name'];
            $configName->description = $data['description'];
            $configName->code = $data['code'];
            $configName->defaultValue = $data['defaultValue'];
            $configName->active = $data['active'];
            $configName->updatedById = $data['updatedById'];
            $configName->createdById = $data['createdById'];
            $configName->modifyDateTime = now();
            $configName->controlText = $data['controlText'];
            $configName->controlminlength = $data['controlminlength'];
            $configName->controlmaxlength = $data['controlmaxlength'];
            $configName->imageMinWidth = $data['imageMinWidth'];
            $configName->imageMinHeight = $data['imageMinHeight'];
            $configName->controlmask = $data['controlmask'];
            $configName->controlTable = $data['controlTable'];
            $configName->controlkey = $data['controlkey'];
            $configName->controlFilterByTenant = $data['controlFilterByTenant'];
            $configName->controlvalue = $data['controlvalue'];
            $configName->isVisible = $data['isVisible'];
            $configName->save();
            return response()->json(['updated', 'Exito'], 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["updated" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $configName = GenConfigName::findOrFail($id);
            $configName->delete();
            return response()->json(["delete" => "Exito"], 200);
        } catch (\PDOException $th) {
            return response()->json(["delete" => "Fail"], 400);
        }
    }
}
