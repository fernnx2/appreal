<?php

namespace App\Http\Controllers\UserMail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserLoginChangePass;
use App\UserLogin;

class UserLoginChangePassController extends Controller
{

    /**
     * Email de informacion que se ha cambiado de password
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userLogin = UserLogin::where('email', '=', $id)->firstOrFail();
            $emailAddress = $userLogin['email'];
            Mail::to($emailAddress)->send(new UserLoginChangePass($userLogin));
            return response()->json(['status send mail' => 'Email de Passowrd Change enviado Exitosamente'], 200);
        } catch (\PDOException $th) {
            return response()->json(['status send email' => 'Fail'], 400);
        }
    }


}
