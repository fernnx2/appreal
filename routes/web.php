<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/image', function () {
    return view('testImage');
});
Route::get('agent', function () {
    return view('ReporteTemplate.agent');
});
Route::get('appoinment', function () {
    return view('ReporteTemplate.appoinment');
});
Route::get('call', function () {
    return view('ReporteTemplate.call');
});
Route::get('listing', function () {
    return view('ReporteTemplate.listing');
});
