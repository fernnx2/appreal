<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContactPhone;

class ContactPhoneType extends Model
{
    protected $table = 'contactPhoneType';
    protected $primaryKey = 'contactPhoneTypeId';
    public $timestamps = false;


    public function phone()
    {
        return $this->hasMany(ContactPhone::class, 'contactPhoneTypeId');
    }

}
