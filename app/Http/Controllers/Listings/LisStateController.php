<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Util\ValidatorController;
use App\LisState;

class LisStateController extends Controller
{
    private $rules = [

        'state' => 'required',
        'updatedById' => 'required',
        'createdById' => 'required'

    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $state = LisState::all();
            return response()->json($state, 200);
        } catch (\PDOException $th) {
            return response()->json(['findAll' => $th], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $state = new LisState();
            $state->lisStatesId = $data['lisStatesId'];
            $state->state = $data['state'];
            $state->description = $data['description'];
            $state->updatedById = $data['updatedById'];
            $state->createdById = $data['createdById'];
            $state->save();
            return response()->json($state, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["created" => "Fail"], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $state = LisState::findOrFail($id);
            return response()->json($state, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $state = LisState::findOrFail($id);
        $validator = new ValidatorController($request, $this->rules);
        $validator->validar();
        try {
            $data = \json_decode($request->getContent(), true);
            $state->lisStatesId = $data['lisStatesId'];
            $state->state = $data['state'];
            $state->description = $data['description'];
            $state->updatedById = $data['updatedById'];
            $state->createdById = $data['createdById'];
            $state->save();
            return response()->json($state, 200);
        } catch (\PDOException $th) {
            //throw $th;
            return response()->json(["update" => "Fail"], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $state = LisState::findOrFail($id);
            $state->delete();
            return response()->json($state, 200);
        } catch (\PDOException $th) {
            return response()->json(['find' => 'fail'], 400);
        }
    }
}
