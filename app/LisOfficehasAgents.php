<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LisOfficehasAgents extends Model
{
    protected $table = 'lisOfficehasAgents';
    protected $primaryKey = 'lisOfficehasAgentsId';
    public $timestamps = false;
}
